<?php
/*
	Name: index.php
	Author: Alex009 (Михайлов Алексей)
	Description: Main file - load engine, start application, show result.
*/

// check time
//$start_time = microtime(true);

// load engine
include 'core/engine.php';

// load application
$app = CFactory::GetApplication();
// set show mode
$app->SetMode(DESKTOP);

// execute
if(file_exists("install"))
{
	// try install application
	if($app->Install()) unlink("install");
}
else
{
	// execute application
	$app->Execute();
}

// show load time
//$end_time = microtime(true) - $start_time;
//echo '<!-- Time: ' . $end_time . '<br>Query: ' . CFactory::GetDatabase()->QueryCount() . ' -->';
?>