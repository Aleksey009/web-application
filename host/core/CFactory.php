<?php
/*
	Name: CFactory.php
	Author: Alex009 (Михайлов Алексей)
	Description: Factory class.
*/

abstract class CFactory
{
	// vars
	private static $config = null;			// configuration array
	private static $application = null;		// CApplication
	private static $modules = array();		// array of CModule
	private static $aliases = null;			// modules aliases array
	private static $session = null;			// CSession
	private static $database = null;		// CDatabase
	private static $mailer = null;			// CMailer
	private static $document = null;		// CDocument

	// methods
	/*
		name:
			GetConfig()
		desc:
			returns array of config from CONFIG_FILE, and read config from file it if it doesn't already readed.
		params:
			-
		retn:
			array		
	*/
	public static function GetConfig()
	{
		if(!self::$config)
		{
			self::$config = parse_ini_file(CONFIG_FILE);
		}
		
		return self::$config;		
	}
	/*
		name:
			GetApplication()
		desc:
			returns CApplication object, and creating it if it doesn't already exist.
		params:
			-
		retn:
			CApplication		
	*/
	public static function GetApplication()
	{
		if(!self::$application)
		{
			// create
			self::$application = new CApplication();
			// initialize
			self::$application->Initialize();
		}

		return self::$application;
	}
	/*
		name:
			GetModule($module_name)
		desc:
			returns CModule object, and creating it if it doesn't already exist.
		params:
			$module_name - name of module
		retn:
			CModule		
	*/
	public static function GetModule($module_name)
	{
		$count = count(self::$modules);
		for($i = 0;$i < $count;$i++)
		{
			if(get_class(self::$modules[$i]) == $module_name)
			{
				return self::$modules[$i];
			}
		}
		
		// create
		self::$modules[$count] = new $module_name();
		// initialize
		self::$modules[$count]->Initialize();
		
		return self::$modules[$count];
	}
	/*
		name:
			GetAliases()
		desc:
			returns array of aliases from ALIASES_FILE, and read aliases from file it if it doesn't already readed.
		params:
			-
		retn:
			array		
	*/
	public static function GetAliases()
	{
		if(!self::$aliases)
		{
			self::$aliases = parse_ini_file(ALIASES_FILE);
		}
		
		return self::$aliases;		
	}
	/*
		name:
			GetSession()
		desc:
			returns CSession object, and creating it if it doesn't already exist.
		params:
			-
		retn:
			CSession		
	*/
	public static function GetSession()
	{
		if(!self::$session)
		{
			// create
			self::$session = new CSession();
			// initialize
			self::$session->Initialize();
		}

		return self::$session;
	}
	/*
		name:
			GetDatabase($notcreate)
		desc:
			returns CDatabase object, and creating it if it doesn't already exist.
		params:
			$notcreate - not create if it doesn't exist
		retn:
			CDatabase		
	*/
	public static function GetDatabase($notcreate = false)
	{
		if(!self::$database)
		{
			if($notcreate) return null;
			// create
			self::$database = new CDatabase();
			// initialize
			self::$database->Initialize();
		}

		return self::$database;
	}
	/*
		name:
			GetMailer()
		desc:
			returns CMailer object, and creating it if it doesn't already exist.
		params:
			-
		retn:
			CMailer		
	*/
	public static function GetMailer()
	{
		if(!self::$mailer)
		{
			// create
			self::$mailer = new CMailer();
			// initialize
			self::$mailer->Initialize();
		}

		return self::$mailer;
	}
	/*
		name:
			GetDocument()
		desc:
			returns CDocument object, and creating it if it doesn't already exist.
		params:
			-
		retn:
			CDocument		
	*/
	public static function GetDocument()
	{
		if(!self::$document)
		{
			// create
			self::$document = new CDocument();
			// initialize
			self::$document->Initialize();
		}

		return self::$document;
	}
}
?>