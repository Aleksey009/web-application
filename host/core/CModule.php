<?php
/*
	Name: CModule.php
	Author: Alex009 (Михайлов Алексей)
	Description: Module abstract class.
*/

abstract class CModule
{
	// vars
	
	// methods
	/*
		name:
			Initialize()
		desc:
			load config
		params:
			-
		retn:
			-		
	*/
	abstract public function Initialize();
	/*
		name:
			Install()
		desc:
			Install module
		params:
			-
		retn:
			true or false
	*/
	abstract public function Install();
	/*
		name:
			Execute($param)
		desc:
			execute module by param
		params:
			$param - param of execution
		retn:
			-
	*/
	abstract public function Execute($param);
	/*
		name:
			IsParamAllowed($param)
		desc:
			check param of execution for module
		params:
			$param - param of execution
		retn:
			true or false
	*/
	abstract public function IsParamAllowed($param);
	/*
		name:
			ExecTemplate($template_name,$params)
		desc:
			execute template
		params:
			$template_name - name of template name
			$params - template params array
		retn:
			-
	*/
	public function ExecTemplate($template_name,$params)
	{
		$config = CFactory::GetConfig();
		$modedir = "desktop";
		if(CFactory::GetApplication()->GetMode() == MOBILE) $modedir = "mobile";
		
		$template = "templates/" . $config["template"] . "/"  . $modedir . "/" . $template_name . $config["template extension"];
		if(!file_exists($template))
		{
			$template = "modules/" . get_class($this) . "/template/" . $modedir . "/" . $template_name . $config["template extension"];
			if(!file_exists($template)) 
			{
				echo "module [" . get_class($this) . "] cant find template [" . $template . "]";
				return;
			}
		}
		CFactory::GetDocument()->ExecuteTemplate($template,$params);
	}
	
	public function IsStandalone()
	{
		return false;
	}
}
?>