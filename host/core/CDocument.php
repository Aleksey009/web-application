<?php
/*
	Name: CDocument.php
	Author: Alex009 (Михайлов Алексей)
	Description: Document class.
*/

class CDocument
{
	// vars
	private $title = "Null document";		// title of document	
	private $description = "Null document";	// description of document
	private $keywords = "";					// keywords of document
	private $generator = "009's engine";	// document's generator	
	private $charset = "utf-8";				// document's charset
	private $css = array();					// array of css
	private $scripts = array();				// array of scripts
	private $start = false;					// flag of capture echo

	private $num = 1;
	
	// methods
	/*
		name:
			Initialize()
		desc:
			connect to mysql database
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
		
	}
	/*
		name:
			AddCss($filename)
		desc:
			add css load to document
		params:
			$filename - filename
		retn:
			-
	*/
	public function AddCss($filename)
	{
		foreach($this->css as $i => $v)
		{
			if($v == $filename) return;
		}
		$this->css[] = $filename;
	}
	/*
		name:
			AddJavaScript($filename)
		desc:
			add java script load to document
		params:
			$filename - filename
		retn:
			-
	*/
	public function AddJavaScript($filename)
	{
		foreach($this->scripts as $i => $v)
		{
			if($v == $filename) return;
		}
		$this->scripts[] = $filename;
	}
	/*
		name:
			SetTitle($title)
		desc:
			set title of page
		params:
			$title - title
		retn:
			-
	*/
	public function SetTitle($title)
	{
		$this->title = $title;
	}
	/*
		name:
			SetDescription($desc)
		desc:
			set description of page
		params:
			$desc - description
		retn:
			-
	*/
	public function SetDescription($desc)
	{
		$this->description = $desc;
	}
	/*
		name:
			ExecuteTemplate($filename)
		desc:
			replace opcodes of file and exec as PHP page
		params:
			$filename - filename
			$params - template params array
		retn:
			-
	*/
	public function ExecuteTemplate($filename,$params)
	{
		// 
		if(!$this->start) 
		{
			ob_start();
			$this->start = true;
		}
		// get file
		$data = file_get_contents($filename);
		// get directiory of media data
		$mediadir = dirname($filename);
		// parse file data
		// {tfolder="path"}
		$data = preg_replace("(\{tfolder\=\"(.*)\"\})","\"http://" . $_SERVER["HTTP_HOST"] . "/" . $mediadir . "/$1\"", $data);
		// execute PHP code
		eval("?>" . $data);
	}
	/*
		name:
			Render()
		desc:
			show completed page
		params:
			-
		retn:
			-
	*/
	public function Render()
	{
		// get buffer
		$buffer = "";
		if($this->start)
		{
			$buffer = ob_get_contents();
			ob_end_clean();
		}
		// head start
		echo "<head>\n";
		// title
		echo "<title>" . $this->title . "</title>\n";
		// description
		echo "<meta name=\"description\" content=\"" . $this->description . "\">\n";
		// keywords
		echo "<meta name=\"keywords\" content=\"" . $this->keywords . "\">\n";
		// generator
		echo "<meta name=\"generator\" content=\"" . $this->generator . "\">\n";
		// charset
		echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=" . $this->charset . "\">\n";
		// css
		echo "<style type=\"text/css\" media=\"all\">\n";
		foreach($this->css as $i => $v) echo "	@import url(\"" . $v . "\");\n";
		echo "</style>\n";
		// scripts
		foreach($this->scripts as $i => $v) echo "<script type=\"text/javascript\" src=\"" . $v . "\"></script>\n";
		// head end
		echo "</head>\n";
		// body
		echo "<body>\n";
		echo $buffer;
		echo "\n</body>\n";
	}
}
?>