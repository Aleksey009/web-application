<?php
/*
	Name: CApplication.php
	Author: Alex009 (Михайлов Алексей)
	Description: Application class.
*/

class CApplication
{
	// vars
	private $params = null;			// array of params
	private $mode = DESKTOP;		// show mode 
	private $module_alias = null;	// array of aliases

	// methods
	/*
		name:
			Initialize()
		desc:
			load config, check site status
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
		// parse address
		if(isset($_SERVER["REDIRECT_URL"]))
		{
			$this->params = explode("/",$_SERVER["REDIRECT_URL"]);
			$count = count($this->params);
			for($i = 0;$i < $count;$i++)
			{
				if(!strlen($this->params[$i])) unset($this->params[$i]);
			}
			$this->params = array_values($this->params);
		}
		// get config
		$config = CFactory::GetConfig();
		// check config
		if(isset($config["site status"]))
		{
			if($config["site status"] == 0)
			{
				echo "site is down";
				die;
			}
		}
	}
	/*
		name:
			Install()
		desc:
			Install application & modules
		params:
			-
		retn:
			true - install complete
			false - fail installation
	*/
	public function Install()
	{
		$result = true;
		// install modules
		echo "start install modules...<br>";
		// read modules install info
		$install_info = parse_ini_file("install_info");
		// start install
		foreach($install_info["modules"] as $i => $v)
		{
			echo "try install module '" . $v . "'...<br>";
			if(CFactory::GetModule($v)->Install()) echo "...module '" . $v . "' installed<br>";
			else
			{
				echo "...module '" . $v . "' not installed<br>";
				$result = false;
			}
		}
		// msg
		if($result) echo "modules installed<br>";
		else echo "modules not installed<br>";
		// complete
		return $result;
	}
	/*
		name:
			Execute()
		desc:
			execute application
		params:
			-
		retn:
			-		
	*/
	public function Execute()
	{
		// params
		$params = array();
		// config
		$config = CFactory::GetConfig();
		// get aliases
		$aliases = CFactory::GetAliases();
		// standart data
		$params["content_module"] = $config["content standart module"];
		$params["content_param"] = $config["content standart param"];
		
		$useTemplate = true;
		
		// check current module
		if(isset($this->params[0])) 
		{
			$params["content_module"] = $this->params[0];
			if(isset($aliases[ $this->params[0] ])) $params["content_module"] = $aliases[ $this->params[0] ];
			$params["content_param"] = "main";
			
			$module = CFactory::GetModule($params["content_module"]);
			
			if(isset($this->params[1]))
			{
				// have param of execution, check it
				if($module->IsParamAllowed($this->params[1]))
				{
					$params["content_param"] = $this->params[1];
				}
			}
			
			if($module->IsStandalone()) {
				$useTemplate = false;
				
				$module->Execute($params["content_param"]);
			}
		}
		
		if($useTemplate) {
			// execute content page
			$this->ExecTemplate("content",$params);
			// render document
			CFactory::GetDocument()->Render();
		}
	}
	/*
		name:
			GetParams()
		desc:
			retn params
		params:
			-
		retn:
			array of params	
	*/
	public function GetParams()
	{
		return $this->params;	
	}
	/*
		name:
			SetMode($mode)
		desc:
			change show mode
		params:
			$mode - show mode
		retn:
			-	
	*/
	public function SetMode($mode)
	{
		$this->mode = $mode;
	}
	/*
		name:
			GetMode()
		desc:
			retn show mode
		params:
			-
		retn:
			show mode	
	*/
	public function GetMode()
	{
		return $this->mode;	
	}
	/*
		name:
			ExecTemplate($template_name,$params)
		desc:
			execute template
		params:
			$template_name - name of template name
			$params - template params array
		retn:
			-
	*/
	public function ExecTemplate($template_name,$params)
	{
		$config = CFactory::GetConfig();
		$modedir = "desktop";
		if(CFactory::GetApplication()->GetMode() == MOBILE) $modedir = "mobile";
		$template = "templates/" . $config["template"] . "/"  . $modedir . "/" . $template_name . $config["template extension"];
		if(file_exists($template)) CFactory::GetDocument()->ExecuteTemplate($template,$params);
		else echo "template [" . $template . "] not exist";
	}
}
?>