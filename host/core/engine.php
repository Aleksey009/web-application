<?php
/*
	Name: engine.php
	Author: Alex009 (Михайлов Алексей)
	Description: Engine main func: autoload classes, main config.
*/

/*
	main config define
*/
define('ENGINE_VERSION','1.0 alpha');
define('ENGINE_AUTHOR','Alex009');
define('ENGINE_SITE','http://localhost/');
define('CONFIG_FILE','config.ini');
define('ALIASES_FILE','aliases.ini');
define('DEBUG',1);
// show modes
define('DESKTOP',1);
define('MOBILE',2);

/*
	name:
		__autoload($class_name)
	desc:
		load not loaded class from directories:
			1. host/[class_name].php
			2. host/core/[class_name].php
			3. host/modules/[class_name]/index.php
	params:
		$class_name - name of class what need load
	retn:
		-
*/
function __autoload($class_name) 
{
	if(file_exists($class_name . ".php")) include_once $class_name . ".php";
	else if(file_exists("core/" . $class_name . ".php")) include_once "core/" . $class_name . ".php";
	else if(file_exists("modules/" . $class_name . "/" . $class_name . ".php")) include_once "modules/" . $class_name . "/" . $class_name . ".php";
}
/*
	name:
		OnError($class_name,$error_desc)
	desc:
		error callback
	params:
		$class_name - name of class or null
		$error_desc - error description
	retn:
		-
*/
function OnError($class_name,$error_desc)
{
	ob_end_clean();
	if($class_name) die("Class: " . $class_name . "<br>Error: " . $error_desc);
	else die("Error: " . $error_desc);
}
/*
	name:
		file_download($filename, $mimetype='application/octet-stream') 
	desc:
		download file
	params:
		$filename - path of file
		$mimetype - mimetype
	retn:
		-
*/
function file_download($filename, $mimetype='application/octet-stream') 
{
	if (file_exists($filename)) 
	{
		// flush all content on buffer
		ob_end_clean();
		// download
		header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK');
		header('Content-Type: ' . $mimetype);
		header('Last-Modified: ' . gmdate('r', filemtime($filename)));
		header('ETag: ' . sprintf('%x-%x-%x', fileinode($filename), filesize($filename), filemtime($filename)));
		header('Content-Length: ' . (filesize($filename)));
		header('Connection: Keep-alive');
		header('Content-Disposition: attachment; filename="' . basename($filename) . '";');
		$f=fopen($filename, 'r');
		while(!feof($f)) 
		{
			echo fread($f, 1024);
			flush();
		}
		fclose($f);
	} 
	else 
	{
		header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
		header('Status: 404 Not Found');
	}
	exit;
}
?>