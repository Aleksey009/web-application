<?php
/*
	Name: CDatabase.php
	Author: Alex009 (Михайлов Алексей)
	Description: DB class.
*/

class CDatabase
{
	// vars
	private $handle = null;			// mysql handle
	private $result = null;			// result of query
	private $query_count = 0;		// count of query

	// methods
	/*
		name:
			Initialize()
		desc:
			connect to mysql database
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
		$config = CFactory::GetConfig();
		// open connection
		$this->handle = mysql_pconnect($config["mysql host"],$config["mysql user"],$config["mysql password"]);
		// db
		mysql_select_db($config["mysql base"], $this->handle);
		// charset
		mysql_query("SET NAMES utf8",$this->handle);
	}
	/*
		name:
			Query($query)
		desc:
			execute query to mysql database
		params:
			format string and params
		retn:
			-	
	*/
	public function Query()
	{
		// get args
		$args = func_get_args();
		// static opcodes
		$args[0] = str_replace("[li]", mysql_insert_id(), $args[0]);
		// get query string pointer
		$query =& $args[0];
		// get format params
		preg_match_all("([isfa]\:[isf]?)",$args[0],$matches);
		// check params
		foreach($args as $i=>$v)
		{
			if($i == 0) continue; // query string
			switch($matches[0][$i - 1])
			{
				case "i:":
				{
					// to int
					$args[$i] = intval($args[$i]);
					break;
				}
				case "s:":
				{
					// to string
					$args[$i] = "'" . mysql_real_escape_string($args[$i],$this->handle) . "'";
					break;
				}
				case "f:":
				{
					// to float
					$args[$i] = floatval ($args[$i]);
					break;
				}
				case "a:":
				{
					$array_str = "";
					// to mixed array
					foreach($args[$i] as $di=>$dv)
					{
						if($di != 0) $array_str .= ",";
						
						if(is_int($dv))	$array_str .= intval($dv);
						if(is_string($dv))	$array_str .= "'" . mysql_real_escape_string($dv,$this->handle) . "'";
						if(is_float($dv))	$array_str .= floatval($dv);
					}
					$args[$i] = $array_str;
					break;
				}
				case "a:i":
				{
					$array_str = "";
					// to integer array
					foreach($args[$i] as $di=>$dv)
					{
						if($di != 0) $array_str .= ",";
						
						$array_str .= intval($dv);
					}
					$args[$i] = $array_str;
					break;
				}
				case "a:s":
				{
					$array_str = "";
					// to string array
					foreach($args[$i] as $di=>$dv)
					{
						if($di != 0) $array_str .= ",";
						
						$array_str .= "'" . mysql_real_escape_string($dv,$this->handle) . "'";
					}
					$args[$i] = $array_str;
					break;
				}
				case "a:f":
				{
					$array_str = "";
					// to float array
					foreach($args[$i] as $di=>$dv)
					{
						if($di != 0) $array_str .= ",";
						
						$array_str .= floatval($dv);
					}
					$args[$i] = $array_str;
					break;
				}
			}
		}
		// change query string to format string
		$query = str_replace("i:", "%d", $query);
		$query = str_replace("s:", "%s", $query);
		$query = str_replace("f:", "%f", $query);
		$query = str_replace("a:i", "%s", $query);
		$query = str_replace("a:s", "%s", $query);
		$query = str_replace("a:f", "%s", $query);
		$query = str_replace("a:", "%s", $query);
		// format query
		$query = call_user_func_array("sprintf", $args);	
		// execute query
		$this->result = mysql_query($query,$this->handle);
		// count
		$this->query_count++;
		// result
		if(!$this->result) return false;
		else return true;
	}
	/*
		name:
			NumRows()
		desc:
			number of rows of result
		params:
			-
		retn:
			int
	*/
	public function NumRows()
	{		
		if($this->result) return mysql_num_rows($this->result);
		else return 0;
	}
	/*
		name:
			LastInsertId()
		desc:
			number of rows of result
		params:
			-
		retn:
			int
	*/
	public function LastInsertId()
	{		
		return mysql_insert_id($this->handle);
	}
	/*
		name:
			Fetch()
		desc:
			fetch result to array
		params:
			-
		retn:
			associative-numeric array	
	*/
	public function Fetch()
	{		
		if($this->result) return mysql_fetch_array($this->result, MYSQL_ASSOC);
		else return null;
	}
	/*
		name:
			Free()
		desc:
			free result
		params:
			-
		retn:
			-
	*/
	public function Free()
	{		
		if($this->result) 
		{
			mysql_free_result($this->result);
			$this->result = null;
		}
	}
	/*
		name:
			GetError()
		desc:
			return error
		params:
			-
		retn:
			error string
	*/
	public function GetError()
	{
		return mysql_error();
	}
	/*
		name:
			Close()
		desc:
			close connection
		params:
			-
		retn:
			-
	*/
	public function Close()
	{
		mysql_close($this->handle);
	}
	/*
		name:
			QueryCount()
		desc:
			count of query
		params:
			-
		retn:
			int
	*/
	public function QueryCount()
	{
		return $this->query_count;
	}
}
?>