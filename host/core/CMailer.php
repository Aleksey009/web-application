<?php
/*
	Name: CMailer.php
	Author: Alex009 (Михайлов Алексей)
	Description: E-Mail class.
*/

class CMailer
{
	// vars

	// methods
	/*
		name:
			Initialize()
		desc:
			connect to mysql database
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
		
	}
	/*
		name:
			CheckEMail($email)
		desc:
			check email valid
		params:
			$email - string email
		retn:
			true or false
	*/
	public function CheckEMail($email)
	{
		if(preg_match("/^[a-zA-Z0-9_]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$]/", $email)) return false;
		/*
		list($Username,$Domain) = split("@",$email);
		
		if(getmxrr($Domain,$MXHost)) return true;
		else if($f = fsockopen($Domain,25,$errno,$errstr,30)) 
		{
			fclose($f);
			return true;
		}
		else return false;
		*/
		return true;
	}
	/*
		name:
			Send($to, $subject, $message)
		desc:
			send mail
		params:
			$to - email address
			$subject - subject of letter
			$message - message
		retn:
			-
	*/
	public function Send($to, $subject, $message)
	{
		mail($to, $subject, $message,"From: test@localhost","Content-Type: text/plain; charset=\"koi8-r\"\n");
	}
}
?>