<?php
/*
	Name: CSession.php
	Author: Alex009 (Михайлов Алексей)
	Description: Session class.
*/

class CSession
{
	// vars
	private $state = "active";		// state: active, expired, destroyed
	private $expire = 15;			// expire time
	private $security = array(		// checks
		"check_browser" => 1,
		"check_address" => 1);

	// methods
	/*
		name:
			Initialize()
		desc:
			check security, start session
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
		// start session
		session_start();
		
		// check security
		if(isset($this->security["check_browser"]))
		{
			// check browser
			if(isset($_SESSION["CSession"]["browser"]))
			{
				if($_SESSION["CSession"]["browser"] != $_SERVER["HTTP_USER_AGENT"])
				{
					// browser changed, destroy session
					$this->state = "destroyed";
					session_destroy();
				}
			}
			else $_SESSION["CSession"]["browser"] = $_SERVER["HTTP_USER_AGENT"];
		}
		if(isset($this->security["check_address"]))
		{
			// check ip address
			if(isset($_SESSION["CSession"]["address"]))
			{
				if($_SESSION["CSession"]["address"] != $_SERVER["REMOTE_ADDR"])
				{
					// address changed, destroy session
					$this->state = "destroyed";
					session_destroy();
				}
			}
			else $_SESSION["CSession"]["address"] = $_SERVER["REMOTE_ADDR"];
		}
		
		// counters
		// expire time
		if(isset($_SESSION["CSession"]["start_time"]))
		{
			if((time() - $_SESSION["CSession"]["start_time"]) > $this->expire * 60)
			{
				// session expired
				$this->state = "expired";
				session_destroy();
			}
			else $_SESSION["CSession"]["start_time"] = time();
		}
		else $_SESSION["CSession"]["start_time"] = time();
		// hits
		if(isset($_SESSION["CSession"]["hits"]))
		{
			$_SESSION["CSession"]["hits"]++;
		}
		else $_SESSION["CSession"]["hits"] = 0;
	}
	/*
		name:
			Get($namespace,$var)
		desc:
			read session var
		params:
			$namespace - var namespace
			$var - var name
		retn:
			var data
	*/
	public function Get($namespace,$var)
	{
		if(!isset($_SESSION[$namespace])) return null;
		if(!isset($_SESSION[$namespace][$var])) return null;
		return $_SESSION[$namespace][$var];
	}
	/*
		name:
			Set($namespace,$var,$data)
		desc:
			write session var
		params:
			$namespace - var namespace
			$var - var name
			$data - var data
		retn:
			-
	*/
	public function Set($namespace,$var,$data)
	{
		$_SESSION[$namespace][$var] = $data;
	}
	/*
		name:
			Reset($namespace,$var)
		desc:
			unset session var
		params:
			$namespace - var namespace
			$var - var name
		retn:
			-
	*/
	public function Reset($namespace,$var)
	{
		if(!isset($_SESSION[$namespace])) return;
		if(!isset($_SESSION[$namespace][$var])) return;
		unset($_SESSION[$namespace][$var]);
	}
}
?>