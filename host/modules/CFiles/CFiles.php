<?php
/*
	Name: CFiles.php
	Author: Alex009 (Михайлов Алексей)
	Description: Files archive module index file.
*/

define('F_FILE_CREATE', "F_FILE_CREATE"); 
define('F_FILE_EDIT', "F_FILE_EDIT"); 
define('F_FILE_DELETE', "F_FILE_DELETE");
define('F_FILES_LIST', "F_FILES_LIST"); 

class CFiles extends CModule
{
	// vars
	private $var = 0;				// array of params
	private $config = null;			// config

	// methods
	/*
		name:
			Initialize()
		desc:
			load config
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
		// read config
		$this->config = parse_ini_file(dirname(__FILE__) . "/config.ini");	
	}
	/*
		name:
			Install()
		desc:
			install module
		params:
			-
		retn:
			true - complete
			false - fail
	*/
	public function Install()
	{
		$result = true;
		$db = CFactory::GetDatabase();
		
		// drop tables
		$db->Query("DROP TABLE IF EXISTS `ca_users_files_links`");
		$db->Query("DROP TABLE IF EXISTS `ca_log`");
		$db->Query("DROP TABLE IF EXISTS `ca_files`");
		$db->Query("DROP TABLE IF EXISTS `ca_users`");
		
		// create tables
		if(!$db->Query("CREATE TABLE `ca_files` 
		(
			`id` INT NOT NULL AUTO_INCREMENT ,
			`name` VARCHAR(64) NULL DEFAULT NULL ,
			`version` VARCHAR(16) NULL DEFAULT NULL ,
			`date` DATETIME NULL ,
			`author` VARCHAR(64) NULL DEFAULT NULL ,
			`description` VARCHAR(4098) NULL DEFAULT NULL ,
			`hash` VARCHAR(32) NULL DEFAULT NULL ,
			`filename` VARCHAR(1024) NULL DEFAULT NULL ,
			PRIMARY KEY (`id`) 
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `ca_files` not created<br>";
			$result = false;
		}
		if(!$db->Query("CREATE TABLE `ca_users` 
		(
			`id` INT NOT NULL AUTO_INCREMENT ,
			`name` VARCHAR(64) NULL ,
			`code` VARCHAR(32) NULL ,
			PRIMARY KEY (`id`) 
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `ca_users` not created<br>";
			$result = false;
		}
		if(!$db->Query("CREATE TABLE `ca_users_files_links` 
		(
			`userid` INT NOT NULL ,
			`fileid` INT NOT NULL ,
			PRIMARY KEY (`userid`, `fileid`) ,
			CONSTRAINT `ca_users_files_links_userid_fk`
				FOREIGN KEY (`userid` )
				REFERENCES `ca_users` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE,
			CONSTRAINT `ca_users_files_links_fileid_fk`
				FOREIGN KEY (`fileid` )
				REFERENCES `ca_files` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `ca_users_files_links` not created<br>";
			$result = false;
		}
		if(!$db->Query("CREATE TABLE `ca_log` 
		(
			`id` INT NOT NULL AUTO_INCREMENT ,
			`userid` INT NULL ,
			`ip` VARCHAR(16) NULL ,
			`moment` DATETIME NULL ,
			`fileid` INT NULL ,
			PRIMARY KEY (`id`) ,
			CONSTRAINT `ca_log_userid_fk`
				FOREIGN KEY (`userid` )
				REFERENCES `ca_users` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE,
			CONSTRAINT `ca_log_fileid_fk`
				FOREIGN KEY (`fileid` )
				REFERENCES `ca_files` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `ca_users` not created<br>";
			$result = false;
		}
		
		// register actions
		CFactory::GetModule("CRole")->RegisterAction(CA_USER_CREATE,"Create new user of closed archive");
		CFactory::GetModule("CRole")->RegisterAction(CA_USER_EDIT,"Edit user of closed archive");
		CFactory::GetModule("CRole")->RegisterAction(CA_USER_DELETE,"Delete user of closed archive");
		CFactory::GetModule("CRole")->RegisterAction(CA_FILE_CREATE,"Create file in closed archive");
		CFactory::GetModule("CRole")->RegisterAction(CA_FILE_EDIT,"Edit file in closed archive");
		CFactory::GetModule("CRole")->RegisterAction(CA_FILE_DELETE,"Delete file from closed archive");
		CFactory::GetModule("CRole")->RegisterAction(CA_LOG_READ,"Read log of downloads from closed archive");
		CFactory::GetModule("CRole")->RegisterAction(CA_USERS_LIST,"Show users from closed archive");
		CFactory::GetModule("CRole")->RegisterAction(CA_FILES_LIST,"Show files from closed archive");
		CFactory::GetModule("CRole")->RegisterAction(CA_USER_FILES,"Grant user privelege to see file of closed archive");
		
		return $result;
	}
	/*
		name:
			Execute($param)
		desc:
			execute module by param
		params:
			$param - param of execution
		retn:
			-
	*/
	public function Execute($param)
	{
		if($param == "main")
		{
			$this->ExecTemplate("closed_archive-main",null);
		}
		else if($param == "list")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// check data
			if(!empty($_POST))
			{
				// set session
				CFactory::GetSession()->Set("closed_archive","code",$_POST["code"]);
				// find user's files by code
				if($db->Query("SELECT *
					FROM ca_files
					WHERE id IN (SELECT uf.fileid 
						FROM ca_users as u, ca_users_files_links as uf
						WHERE u.id = uf.userid AND u.code = s:)",$_POST["code"]))
				{
					$params["count"] = 0;
					$params["data"] = array();
					// get from files db
					while($data = $db->Fetch()) 
					{
						$params["data"][] = $data;
						$params["count"]++;
					}
					$db->Free();
					// complete
					$this->ExecTemplate("closed_archive-list",$params);
					return;
				}
				else $params["errors"]["ERROR_DB_ERROR"] = true;
			}
			else
			{
				// load template
				$this->ExecTemplate("closed_archive-list-input",$params);
				return;
			}
			// load template
			$this->ExecTemplate("closed_archive-list-error",$params);
			return;
		}
		else if($param == "get")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// get data
			$code = CFactory::GetSession()->Get("closed_archive","code");
			$file = $_GET["id"];
			// check access
			if($db->Query("SELECT f.id as f_id,
					f.filename as f_filename,
					u.id as u_id
				FROM ca_files as f, ca_users as u, ca_users_files_links as uf 
				WHERE f.id = uf.fileid AND u.id = uf.userid AND uf.fileid = i: AND u.code = s:",$file,$code))
			{
				if($data = $db->Fetch())
				{
					$id = $data["u_id"];
					$fileid = $data["f_id"];
					$db->Free();
					// log download
					$db->Query("INSERT INTO ca_log (`userid`,`ip`,`moment`,`fileid`) VALUES (i:,s:,NOW(),i:)",$id,$_SERVER["REMOTE_ADDR"],$fileid);
					// start download
					file_download("modules_data/CClosedArchive/" . $data["f_filename"]);
				}
				else $params["errors"]["ERROR_ROW_NOT_FOUND"] = true;
			}
			else $params["errors"]["ERROR_DB_ERROR"] = true;
			// load template
			$this->ExecTemplate("closed_archive-get-error",$params);
			return;
		}
		else if($param == "control")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// get user info
			$userid = CFactory::GetSession()->Get("global","userid");
			if(!$userid) $params["errors"]["ERROR_NOT_LOGIN"] = true;
			else
			{
				if(CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,CA_USER_CREATE)) $params["actions"]["CA_USER_CREATE"] = true;
				if(CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,CA_USER_EDIT)) $params["actions"]["CA_USER_EDIT"] = true;
				if(CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,CA_USER_DELETE)) $params["actions"]["CA_USER_DELETE"] = true;
				if(CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,CA_FILE_CREATE)) $params["actions"]["CA_FILE_CREATE"] = true;
				if(CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,CA_FILE_EDIT)) $params["actions"]["CA_FILE_EDIT"] = true;
				if(CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,CA_FILE_DELETE)) $params["actions"]["CA_FILE_DELETE"] = true;
				if(CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,CA_LOG_READ)) $params["actions"]["CA_LOG_READ"] = true;
				if(CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,CA_USERS_LIST)) $params["actions"]["CA_USERS_LIST"] = true;
				if(CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,CA_FILES_LIST)) $params["actions"]["CA_FILES_LIST"] = true;
				if(CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,CA_USER_FILES)) $params["actions"]["CA_USER_FILES"] = true;
				
				if(!isset($_GET["action"]))
				{					
					if(!isset($params["actions"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{
						// load template
						$this->ExecTemplate("closed_archive-control",$params);
						return;
					}
				}
				else if($_GET["action"] == "user_create")
				{
					// check access
					if(!isset($params["actions"]["CA_USER_CREATE"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{
						// create user
						if(!empty($_POST))
						{
							// add data
							if($db->Query("INSERT INTO ca_users (`name`,`code`) VALUES (s:,s:)",$_POST["name"],$_POST["code"]))
							{
								// load template
								$this->ExecTemplate("closed_archive-control-user_create-complete",$params);
								return;
							}
							else $params["errors"]["ERROR_DB_ERROR"] = true;
						}
						else
						{
							// load template
							$this->ExecTemplate("closed_archive-control-user_create",$params);
							return;
						}
					}
				}
				else if($_GET["action"] == "user_edit")
				{
					// check access
					if(!isset($params["actions"]["CA_USER_EDIT"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{
						if(!empty($_POST))
						{
							// add data
							if($db->Query("UPDATE ca_users SET `name`=s:,`code`=s: WHERE `id`=i:",$_POST["name"],$_POST["code"],$_GET["id"]))
							{
								// load template
								$this->ExecTemplate("closed_archive-control-user_edit-complete",$params);
								return;
							}
							else $params["errors"]["ERROR_DB_ERROR"] = true;
						}
						else
						{
							// get list
							if($db->Query("SELECT * FROM ca_users WHERE id=i:",$_GET["id"]))
							{
								if($db->NumRows() == 0) $params["errors"]["ERROR_ROW_NOT_FOUND"] = true;
								else
								{
									if($data = $db->Fetch()) $params["userdata"] = $data;
									$db->Free();
									
									// load template
									$this->ExecTemplate("closed_archive-control-user_edit",$params);
									return;
								}
							}
							else $params["errors"]["ERROR_DB_ERROR"] = true;
						}
					}
				}
				else if($_GET["action"] == "user_delete")
				{
					// check access
					if(!isset($params["actions"]["CA_USER_DELETE"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{	
						// add data
						if($db->Query("DELETE FROM ca_users WHERE `id`=i:",$_GET["id"]))
						{
							// load template
							$this->ExecTemplate("closed_archive-control-user_delete-complete",$params);
							return;
						}
						else $params["errors"]["ERROR_DB_ERROR"] = true;
					}
				}
				else if($_GET["action"] == "file_create")
				{
					// check access
					if(!isset($params["actions"]["CA_FILE_CREATE"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{
						// create file
						if(!empty($_POST))
						{
							if($_FILES["userfile"]["error"] == UPLOAD_ERR_OK)
							{
								$hash = md5_file($_FILES["userfile"]["tmp_name"]);
								// check existing
								$num = 0;
								$filename = $_FILES["userfile"]["name"];
								if(file_exists("modules_data/CClosedArchive/" . $filename))
								{
									while(file_exists("modules_data/CClosedArchive/" . $num . "_" . $filename)) $num++;
									$filename = $num . "_" . $_FILES["userfile"]["name"];
								}
								// move file
								if(move_uploaded_file($_FILES["userfile"]["tmp_name"], "modules_data/CClosedArchive/" . $filename))
								{
									// add data
									if($db->Query("INSERT INTO ca_files 
											(`name`,`version`,`date`,`author`,`description`,`hash`,`filename`) 
										VALUES 
											(s:,s:,NOW(),s:,s:,s:,s:)",
											$_POST["name"],$_POST["version"],$_POST["author"],$_POST["description"],$hash,$filename))
									{
										// load template
										$this->ExecTemplate("closed_archive-control-file_create-complete",$params);
										return;
									}
									else $params["errors"]["ERROR_DB_ERROR"] = true;
								}
							}
							else $params["errors"]["ERROR_FILE_UPLOAD"] = true;
						}
						else
						{
							// load template
							$this->ExecTemplate("closed_archive-control-file_create",$params);
							return;
						}
					}
				}
				else if($_GET["action"] == "file_edit")
				{
					// check access
					if(!isset($params["actions"]["CA_FILE_EDIT"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{
						// create file
						if(!empty($_POST))
						{
							// save data
							if($db->Query("UPDATE ca_files SET `name`=s:, `version`=s:, `author`=s:, `description`=s: WHERE `id`=i:",
									$_POST["name"],$_POST["version"],$_POST["author"],$_POST["description"],$_GET["id"]))
							{
								// load template
								$this->ExecTemplate("closed_archive-control-file_edit-complete",$params);
								return;
							}
							else $params["errors"]["ERROR_DB_ERROR"] = true;
						}
						else
						{
							// get current data
							if($db->Query("SELECT * FROM ca_files WHERE id=i:",$_GET["id"]))
							{
								if($db->NumRows() == 0) $params["errors"]["ERROR_ROW_NOT_FOUND"] = true;
								else
								{
									if($data = $db->Fetch()) $params["filedata"] = $data;
									$db->Free();
									
									// load template
									$this->ExecTemplate("closed_archive-control-file_edit",$params);
									return;
								}
							}
							else $params["errors"]["ERROR_DB_ERROR"] = true;
						}
					}
				}
				else if($_GET["action"] == "file_delete")
				{
					// check access
					if(!isset($params["actions"]["CA_FILE_DELETE"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{
						// get filename
						if($db->Query("SELECT `filename` FROM ca_files WHERE `id`=i:",$_GET["id"]))
						{
							$data = $db->Fetch();
							$db->Free();
							$filename = "modules_data/CClosedArchive/" . $data["filename"];
							
							if(file_exists($filename)) unlink($filename);
						}
						// delete data
						if($db->Query("DELETE FROM ca_files WHERE `id`=i:",$_GET["id"]))
						{
							// load template
							$this->ExecTemplate("closed_archive-control-file_delete-complete",$params);
							return;
						}
						else $params["errors"]["ERROR_DB_ERROR"] = true;
					}
				}
				else if($_GET["action"] == "log_read")
				{
					// check access
					if(!isset($params["actions"]["CA_LOG_READ"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{
						if($db->Query("SELECT 
								f.id as fileid,
								f.name as filename,
								f.hash as filehash,
								f.filename as filepath,
								u.name as username,
								u.code as usercode,
								l.ip as ip,
								l.moment as moment
							FROM
								ca_files as f,
								ca_users as u,
								ca_log as l
							WHERE
								f.id = l.fileid AND u.id = l.userid
							ORDER BY l.moment DESC"))
						{
							$params["logsize"] = $db->NumRows();
							while($data = $db->Fetch()) $params["logs"][] = $data;
							$db->Free();
							
							// load template
							$this->ExecTemplate("closed_archive-control-log_read",$params);
							return;
						}
						else $params["errors"]["ERROR_DB_ERROR"] = true;
					}
				}
				else if($_GET["action"] == "users_list")
				{
					// check access
					if(!isset($params["actions"]["CA_USERS_LIST"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{
						// get start pos
						$params["start"] = 0;
						if(isset($_GET["start"])) $params["start"] = $_GET["start"];
						// per page
						$params["per_page"] = $this->config["users per page"];
						// get list
						if($db->Query("SELECT * 
							FROM ca_users 
							ORDER BY `id` 
							LIMIT i:,i:",$params["start"],$params["per_page"]))
						{
							$params["userscount"] = $db->NumRows();
							while($data = $db->Fetch())
							{
								$params["usersdata"][] = $data;
							}
							$db->Free();
							// load template
							$this->ExecTemplate("closed_archive-control-users_list",$params);
							return;
						}
						else $params["errors"]["ERROR_DB_ERROR"] = true;
					}
				}
				else if($_GET["action"] == "files_list")
				{
					// check access
					if(!isset($params["actions"]["CA_FILES_LIST"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{
						// get start pos
						$params["start"] = 0;
						if(isset($_GET["start"])) $params["start"] = $_GET["start"];
						// per page
						$params["per_page"] = $this->config["files per page"];
						// get files list
						if($db->Query("SELECT * 
							FROM ca_files 
							ORDER BY `date` DESC
							LIMIT i:,i:",$params["start"],$params["per_page"]))
						{
							$params["filescount"] = $db->NumRows();
							while($data = $db->Fetch())
							{
								$params["filesdata"][] = $data;
							}
							$db->Free();
							// load template
							$this->ExecTemplate("closed_archive-control-files_list",$params);
							return;
						}
						else $params["errors"]["ERROR_DB_ERROR"] = true;
					}
				}
				else if($_GET["action"] == "user_files")
				{
					// check access
					if(!isset($params["actions"]["CA_USER_FILES"])) $params["errors"]["ERROR_NOT_ACCESS"] = true;
					else
					{
						// edit allowed files for user
						if(!empty($_POST))
						{
							if(isset($_POST["all_files"]))
							{
								// insert new data
								foreach($_POST["all_files"] as $i => $v)
								{
									if($v == "0" && isset($_POST["files"][$i]))
									{
										if(!$db->Query("INSERT INTO ca_users_files_links (`userid`,`fileid`) 
											VALUES (i:,i:)",$_GET["id"],$i)) $params["errors"]["ERROR_DB_ERROR"] = true;
									}
									else if($v == "1" && (!isset($_POST["files"][$i])))
									{
										if(!$db->Query("DELETE FROM ca_users_files_links 
											WHERE userid = i: 
												AND fileid = i:",$_GET["id"],$i)) $params["errors"]["ERROR_DB_ERROR"] = true;
									}
								}
							}
							if(!isset($params["errors"]["ERROR_DB_ERROR"]))
							{
								// load template
								$this->ExecTemplate("closed_archive-control-user_files-complete",$params);
								return;
							}
						}
						else
						{
							// get start pos
							$params["start"] = 0;
							if(isset($_GET["start"])) $params["start"] = $_GET["start"];
							// per page
							$params["per_page"] = $this->config["files per page"];
							// get files list
							if($db->Query("SELECT * 
								FROM ca_files 
								ORDER BY `date` DESC
								LIMIT i:,i:",$params["start"],$params["per_page"]))
							{
								$params["filescount"] = $db->NumRows();
								while($data = $db->Fetch())
								{
									$params["filesdata"][] = $data;
								}
								$db->Free();
								// get user's files
								if($db->Query("SELECT * 
									FROM ca_users_files_links
									WHERE userid = i:",$_GET["id"]))
								{
									while($data = $db->Fetch())
									{
										$params["userfiles"][ $data["fileid"] ] = true;
									}
									$db->Free();
									// load template
									$this->ExecTemplate("closed_archive-control-user_files",$params);
									return;
								}
								else $params["errors"]["ERROR_DB_ERROR"] = true;
							}
							else $params["errors"]["ERROR_DB_ERROR"] = true;
						}
					}
				}
			}
			// load template
			$this->ExecTemplate("closed_archive-control-error",$params);
			return;
		}
	}
	/*
		name:
			IsParamAllowed($param)
		desc:
			check param of execution for module
		params:
			$param - param of execution
		retn:
			true or false
	*/
	public function IsParamAllowed($param)
	{
		$allowed = array(
				"list",
				"get",
				"control"
			);
		if(in_array($param,$allowed)) return true;
		else return false;
	}
}
?>