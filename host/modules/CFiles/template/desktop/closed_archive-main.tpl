<?php
/*
	Name:	CUser main template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		categories - array of categories data from db
		errors - errors array
	Errors:
		ERROR_DB_ERROR
*/
?>

<div title="Categories">
<?php 
function ShowCategoriesWithParent($categories,$parentid,$prefix)
{
	foreach($categories as $i => $data)
	{
		if($data["parentid"] == $parentid)
		{
			echo $prefix . " <a href=\"http://cmsdev/category/info?id=" . $data["id"] . "\">" . $data["name"] . "</a><br>\n";
			ShowCategoriesWithParent($categories,$data["id"],$prefix . "#");
		}
	}
}

ShowCategoriesWithParent($params["categories"],NULL,"");
?>
</div>