<?php
/*
	Name: CLicenses.php
	Author: Alex009 (Михайлов Алексей)
	Description: License module.
*/

class CLicenses extends CModule
{
	// vars
	private $seed = 0;
	
	// methods
	/*
		name:
			Initialize()
		desc:
			load config
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
		
	}
	/*
		name:
			Install()
		desc:
			Install module
		params:
			-
		retn:
			true or false
	*/
	public function Install()
	{
		$result = true;
		$db = CFactory::GetDatabase();
		// drop tables
		$db->Query("DROP TABLE IF EXISTS `l_licenses`");
		$db->Query("DROP TABLE IF EXISTS `l_logs`");
		
		// create tables
		if(!$db->Query("CREATE TABLE `l_licenses` 
		(
			`id` INT NOT NULL AUTO_INCREMENT ,
			`key` VARCHAR(64) NULL ,
			`name` VARCHAR(128) NULL ,
			`active` BOOL ,
			PRIMARY KEY (`id`) 
		)  DEFAULT CHARSET=utf8")) 
		{
			echo "Table `l_licenses` not created<br>";
			$result = false;
		}
		if(!$db->Query("CREATE TABLE `l_logs` 
		(
			`id` INT NOT NULL AUTO_INCREMENT ,
			`license_id` INT NOT NULL, 
			`date` DATETIME NULL ,
			`location` VARCHAR(256) NULL ,
			`accept` BOOL DEFAULT 1 ,
			PRIMARY KEY (`id`),
			CONSTRAINT `l_logs_license_id_fk`
				FOREIGN KEY (`license_id` )
				REFERENCES `l_licenses` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE
		)  DEFAULT CHARSET=utf8")) 
		{
			echo "Table `l_logs` not created<br>";
			$result = false;
		}
		
		return $result;
	}
	/*
		name:
			Execute($param)
		desc:
			execute module by param
		params:
			$param - param of execution
		retn:
			-
	*/
	public function Execute($param)
	{
		if($param == "check")
		{
			$db = CFactory::GetDatabase();
			
			date_default_timezone_set('UTC');
			
			$date = mktime(0, 0, 0);
			$data = $_POST["data"];
			$loc = $_POST["loc"];
			
			if($data == null) {
				//return;
			}
			
			$this->seed = (int)$date;
			
			preg_match_all('/(\d+)/', $data, $matches);
			
			$key = '';
			foreach($matches[0] as $num) {
				$fix = abs($this->nextLong() % 64);
				$num = $num - $fix;
				$key = $key . chr($num);
			}
			
			
			$this->seed = (int)$date;
			
			preg_match_all('/(\d+)/', $loc, $matches);
			
			$location = $_SERVER['REMOTE_ADDR'] . '@';
			foreach($matches[0] as $num) {
				$fix = abs($this->nextLong() % 64);
				$num = $num - $fix;
				$location = $location . chr($num);
			}
			
			$accept_key = false;
			$license_id = 0;
			
			if($db->Query("SELECT `id` 
								FROM `l_licenses` 
								WHERE `key` = s: 
								AND `active` = 1", $key)) {
				if($data = $db->Fetch()) {
					$license_id = $data["id"];
					
					if($db->Query("SELECT COUNT(*) AS count 
										FROM `l_logs` 
										WHERE `license_id` = i:", $license_id)) {
						if($data = $db->Fetch()) {
							
							if($data["count"] > 0) {
								if($db->Query("SELECT COUNT(*) AS count 
													FROM `l_logs` 
													WHERE `license_id` = i: 
														AND `location` = s:
														AND `accept` = 1", $license_id, $location)) {
									if($data = $db->Fetch()) {
										if($data["count"] > 0) {
											$accept_key = true;
										}
									}
								}
							}
							else {
								$accept_key = true;
							}
						}
					}
				}
			}
			$db->Free();
			
			$db->Query("INSERT INTO `l_logs` 
							(`license_id`, `date`, `location`, `accept`) 
						VALUES 
							(i:, SYSDATE(), s:, i:)", $license_id, $location, ($accept_key ? 1 : 0));
			
			if($accept_key) {
				echo gzcompress(file_get_contents("./data/valid_response.json"));
			}
			else {
				echo gzcompress(file_get_contents("./data/invalid_response.json"));
			}
			return;
		}
	}
	/*
		name:
			IsParamAllowed($param)
		desc:
			check param of execution for module
		params:
			$param - param of execution
		retn:
			true or false
	*/
	public function IsParamAllowed($param)
	{
		$allowed = array(
				"check",
			);
		if(in_array($param,$allowed)) return true;
		else return false;
	}
	
	public function IsStandalone()
	{
		return true;
	}
	
	private function nextLong()
	{
		$this->seed = $this->seed + 5;
		if($this->seed > 2147483000) {
			$this->seed -= 2147483000;
		}
		return $this->seed;
	}
}
?>