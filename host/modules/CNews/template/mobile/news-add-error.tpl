<?php
/*
	Name:	News add error template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		$errors - array of errors
	Constants:
		$ERROR_NOT_LOGIN
		$ERROR_EMPTY_HEADER
		$ERROR_EMPTY_SHORT_TEXT
		$ERROR_DB_ERROR
*/
?>

<center>
<?php 
if(isset($errors[$ERROR_NOT_LOGIN])) echo "not login<br>";
if(isset($errors[$ERROR_EMPTY_HEADER])) echo "empty header<br>";
if(isset($errors[$ERROR_EMPTY_SHORT_TEXT])) echo "empty short text<br>";
if(isset($errors[$ERROR_DB_ERROR])) echo $db->GetError() . "<br>";
?>
</center>