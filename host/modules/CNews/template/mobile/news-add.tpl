<?php
/*
	Name:	Content page template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		$content_module - current selected module
		$content_param - current param of selected module
*/
?>

<div>
	<h2>Добавить новость</h2>
</div>
<form method="post">
	<table width="100%">
		<tr>
			<td>
				Заголовок:
			</td>
			<td>
				<input type="text" name="header" maxlength="64">
			</td>
		</tr>
		<tr>
			<td>
				Категория:
			</td>
			<td>
				Пока пусто
			</td>
		</tr>
		<tr>
			<td>
				<b>Короткое описание:</b> (Обязательно)
			</td>
			<td>
				<textarea name="short_text" cols=80 rows=10></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<b>Полное описание:</b> (Необязательно)
			</td>
			<td>
				<textarea name="text" cols=80 rows=10></textarea>
			</td>
		</tr>
	</table>
	<div>
		<input type="submit" name="submit" value="Add">
	</div>
</form>