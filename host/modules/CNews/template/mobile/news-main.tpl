<?php
/*
	Name:	News main template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		newsdata - data from db:
			`id`
			`date`
			`header`
			`short_text`
			`author_id`
			`author_login`	
		count - current number on page
*/

CFactory::GetDocument()->AddCss({tfolder="css/news.css"});
?>

<div class="news">
	<div class="head">
		<a href="/news/read?id=<?php echo $params["newsdata"]["id"] . "-" . $params["newsdata"]["header"];?>"><?php echo $params["newsdata"]["header"];?></a>
		<span class="date"><?php echo $params["newsdata"]["date"];?></span>
	</div>
	<div class="content">
		<?php echo $params["newsdata"]["short_text"];?>
	</div>
	<div>
		<?php echo $params["newsdata"]["author_login"];?>
	</div>
</div>