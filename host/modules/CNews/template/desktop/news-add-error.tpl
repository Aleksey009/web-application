<?php
/*
	Name:	News add error template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		errors - array of errors
	Errors:
		ERROR_NOT_LOGIN
		ERROR_EMPTY_HEADER
		ERROR_EMPTY_SHORT_TEXT
		ERROR_DB_ERROR
*/
?>

<center>
Errors:
<?php 
if(isset($params["errors"]["ERROR_NOT_LOGIN"])) echo "not login<br>";
if(isset($params["errors"]["ERROR_EMPTY_HEADER"])) echo "empty header<br>";
if(isset($params["errors"]["ERROR_EMPTY_SHORT_TEXT"])) echo "empty short text<br>";
if(isset($params["errors"]["ERROR_DB_ERROR"])) echo CFactory::GetDatabase()->GetError() . "<br>";
?>
</center>