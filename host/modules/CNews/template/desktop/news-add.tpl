<?php
/*
	Name:	CNews add page template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		categorydata - array of categories data from db
		categorycount - count of categories
*/
?>

<div>
	<h2>Добавить новость</h2>
</div>
<form method="post">
	<table width="100%">
		<tr>
			<td>
				Заголовок:
			</td>
			<td>
				<input type="text" name="header" maxlength="64">
			</td>
		</tr>
		<tr>
			<td>
				Категория:
			</td>
			<td>
				<select name="category">
					<?php 
						foreach($params["categorydata"] as $i => $v) echo "<option value=" . $v["id"] . ">" . $v["name"] . "</option>";
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<b>Короткое описание:</b> (Обязательно)
			</td>
			<td>
				<textarea name="short_text" cols=80 rows=10></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<b>Полное описание:</b> (Необязательно)
			</td>
			<td>
				<textarea name="text" cols=80 rows=10></textarea>
			</td>
		</tr>
	</table>
	<div>
		<input type="submit" name="submit" value="Add">
	</div>
</form>