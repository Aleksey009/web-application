<?php
/*
	Name: CNews.php
	Author: Alex009 (Михайлов Алексей)
	Description: News module index file.
*/

// constants
define('NEWS_ADD'	, "NEWS_ADD"); 
define('NEWS_DELETE', "NEWS_DELETE");
define('NEWS_READ'	, "NEWS_READ"); 
define('NEWS_LIST'	, "NEWS_LIST");  


class CNews extends CModule
{
	// vars
	private $config = null;				// config

	// methods
	/*
		name:
			Initialize()
		desc:
			load config
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
		// read config
		$this->config = parse_ini_file(dirname(__FILE__) . "/config.ini");
	}
	/*
		name:
			Install()
		desc:
			install module
		params:
			-
		retn:
			true - complete
			false - fail	
	*/
	public function Install()
	{
		$result = true;
		$db = CFactory::GetDatabase();
		// drop table
		$db->Query("DROP TABLE `n_news_category_links`");
		$db->Query("DROP TABLE `n_news`");
		// create table
		if(!$db->Query("CREATE TABLE `n_news` 
		(
			`id` INT NOT NULL ,
			`userid` INT NULL ,
			`date` DATETIME NULL ,
			`header` VARCHAR(128) NULL ,
			`short_text` VARCHAR(1024) NULL ,
			`text` VARCHAR(4098) NULL ,
			PRIMARY KEY (`id`) ,
			CONSTRAINT `n_news_userid_fk`
				FOREIGN KEY (`userid` )
				REFERENCES `u_user` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `n_news` not created<br>";
			$result = false;
		}
		if(!$db->Query("CREATE TABLE `n_news_category_links` 
		(
			`newsid` INT NOT NULL ,
			`categoryid` INT NOT NULL ,
			PRIMARY KEY (`newsid`, `categoryid`) ,
			CONSTRAINT `n_news_category_links_newsid_fk`
				FOREIGN KEY (`newsid` )
				REFERENCES `n_news` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE,
			CONSTRAINT `n_news_category_links_categoryid_fk`
				FOREIGN KEY (`categoryid` )
				REFERENCES `c_category` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `n_news_category_links` not created<br>";
			$result = false;
		}
		// register actions
		CFactory::GetModule("CRole")->RegisterAction(NEWS_ADD,"Add news");
		CFactory::GetModule("CRole")->RegisterAction(NEWS_DELETE,"Delete exist news");
		CFactory::GetModule("CRole")->RegisterAction(NEWS_READ,"Read exist news");
		CFactory::GetModule("CRole")->RegisterAction(NEWS_LIST,"See list of exist news");
		
		return $result;
	}
	/*
		name:
			Execute($param)
		desc:
			execute module by param
		params:
			$param - param of execution
		retn:
			-
	*/
	public function Execute($param)
	{
		if(($param == "main") or ($param == "list"))
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// get user id
			$userid = CFactory::GetSession()->Get("global","userid");
			// check action
			if(!CFactory::GetModule("CRole")->IsActionAllowedForUser($userid,NEWS_LIST))
			{
				// set error data
				$params["errors"]["ERROR_NOT_ACCESS"] = true;
				// load template
				$this->ExecTemplate("news-main-error",$params);
				return;
			}
			// get start pos
			$params["start"] = 0;
			if(isset($_GET["start"])) $params["start"] = $_GET["start"];
			// per page
			$params["per_page"] = $this->config["news per page"];
			// show last added news
			if($db->Query("SELECT 
				`n_news`.`id` AS `id`,
				DATE_FORMAT(`n_news`.`date`,'%%W, %%e %%M %%Y, %%k:%%i') AS `date`,
				`n_news`.`header` AS `header`,
				`category`.`id` AS `cat_id`,
				`category`.`name` AS `cat_name`,
				`n_news`.`short_text` AS `short_text`,
				`user`.`id` AS `author_id`,
				`user`.`login` AS `author_login`				
				FROM `n_news`,`user`,`category`,`n_news_category_links`
				WHERE `n_news`.`userid` = `user`.`id` 
					AND `n_news_category_links`.`newsid` = `n_news`.`id` 
					AND `n_news_category_links`.`categoryid` = `category`.`id`
				ORDER BY `n_news`.`date` DESC
				LIMIT i:,i:",$params["start"],$params["per_page"]))
			{
				while($data = $db->Fetch())
				{
					$params["newsdata"][] = $data;
					$params["showed_news"]++;
				}
			}
			else $params["errors"]["ERROR_DB_ERROR"] = true;
			// get count of news
			if($db->Query("SELECT 
				COUNT(*) as 'count'				
				FROM `n_news`"))
			{
				$data = $db->Fetch();
				$params["news_count"] = $data["count"];
			}
			else $params["errors"]["ERROR_DB_ERROR"] = true;
			// check errors
			if(empty($params["errors"]))
			{
				// load template
				$this->ExecTemplate("news-main",$params);
			}
			else
			{
				// load template
				$this->ExecTemplate("news-main-error",$params);
			}
			// complete
			return;
		}
		else if($param == "add")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// get user id
			$userid = CFactory::GetSession()->Get("global","userid");
			// get role
			$roleid = CFactory::GetModule("CUser")->GetUserRole($userid);
			// get user info
			$userinfo = CFactory::GetModule("CUser")->GetUserData($userid);
			// check action
			if(!CFactory::GetModule("CRole")->IsActionAllowedForRole($roleid,NEWS_ADD))
			{
				// set error data
				$params["errors"]["ERROR_NOT_ACCESS"] = true;
				// load template
				$this->ExecTemplate("news-add-error",$params);
				return;
			}
			// check data
			if(!empty($_POST))
			{
				// check input data
				// header
				if(!strlen($_POST["header"])) $params["errors"]["ERROR_EMPTY_HEADER"] = true;
				// short text
				if(!strlen($_POST["short_text"])) $params["errors"]["ERROR_EMPTY_SHORT_TEXT"] = true;
				// text
				if(!strlen($_POST["text"])) $_POST["text"] = $_POST["short_text"];
				// check errors exists
				if(empty($params["errors"]))
				{
					// add data to db
					if($db->Query("INSERT INTO `n_news`
						(`userid`,`date`,`header`,`short_text`,`text`)
						VALUES
						(i:,NOW(),s:,s:,s:)",
						CFactory::GetSession()->Get("global","userid"),
						$_POST["header"],
						nl2br($_POST["short_text"]),
						nl2br($_POST["text"]))
						&&
						$db->Query("INSERT INTO `n_news_category_links`
						(`newsid`,`categoryid`)
						VALUES
						([li],i:)",$_POST["category"]))
					{
						// load template
						$this->ExecTemplate("news-add-done",null);
						// complete
						return;
					}
					else $params["errors"]["ERROR_DB_ERROR"] = true;
				}
				// load template
				$this->ExecTemplate("news-add-error",$params);
				// complete
				return;
			}
			else
			{
				// get category list
				if($db->Query("SELECT 
					*				
					FROM `category`"))
				{
					while($data = $db->Fetch())
					{
						$params["categorydata"][] = $data;
						$params["categorycount"]++;
					}
				}
				// load template
				$this->ExecTemplate("news-add",$params);
				// complete
				return;
			}
		}
		else if($param == "read")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// get user id
			$userid = CFactory::GetSession()->Get("global","userid");
			// get role
			$roleid = CFactory::GetModule("CUser")->GetUserRole($userid);
			// get user info
			$userinfo = CFactory::GetModule("CUser")->GetUserData($userid);
			// check action
			if(!CFactory::GetModule("CRole")->IsActionAllowedForRole($roleid,NEWS_READ))
			{
				// set error data
				$params["errors"]["ERROR_NOT_ACCESS"] = true;
				// load template
				$this->ExecTemplate("news-read-error",$params);
				return;
			}
			// get start pos
			$newsid = $_GET["id"];
			// per page
			$params["per_page"] = $this->config["news per page"];
			// show last added news
			if($db->Query("SELECT 
				`n_news`.`id` AS `id`,
				DATE_FORMAT(`n_news`.`date`,'%%W, %%e %%M %%Y, %%k:%%i') AS `date`,
				`n_news`.`header` AS `header`,
				`category`.`id` AS `cat_id`,
				`category`.`name` AS `cat_name`,
				`n_news`.`short_text` AS `short_text`,
				`n_news`.`text` AS `text`,
				`user`.`id` AS `author_id`,
				`user`.`login` AS `author_login`				
				FROM `n_news`,`user`,`category`,`n_news_category_links`
				WHERE `n_news`.`userid` = `user`.`id` 
					AND `n_news_category_links`.`newsid` = `n_news`.`id` 
					AND `n_news_category_links`.`categoryid` = `category`.`id`
					AND `n_news`.`id` = i:",$newsid))
			{
				$params["newsdata"] = $db->Fetch();
			}
			else $params["errors"]["ERROR_DB_ERROR"] = true;
			// check errors
			if(empty($params["errors"]))
			{
				// load template
				$this->ExecTemplate("news-read",$params);
			}
			else
			{
				// load template
				$this->ExecTemplate("news-read-error",$params);
			}
			// complete
			return;
		}
	}
	/*
		name:
			IsParamAllowed($param)
		desc:
			check param of execution for module
		params:
			$param - param of execution
		retn:
			true or false
	*/
	public function IsParamAllowed($param)
	{
		$allowed = array(
				"list",
				"add",
				"edit",
				"read",
				"delete",
				"category"
			);
		if(in_array($param,$allowed)) return true;
		else return false;
	}
}
?>