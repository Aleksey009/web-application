<?php
/*
	Name:	CUser register error template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		errors - array of errors
		login - reg login
		password - reg password
	Errors:
		ERROR_EMPTY_EMAIL
		ERROR_INVALID_EMAIL
		ERROR_EXIST_EMAIL
		ERROR_EMPTY_LOGIN
		ERROR_INVALID_LOGIN
		ERROR_EXIST_LOGIN
		ERROR_EMPTY_PASSWORD
		ERROR_SHORT_PASSWORD
		ERROR_DB_ERROR
*/
?>

<center>
Errors: 
<?php 
	echo var_dump($params["errors"]); 
	if(isset($params["errors"]["ERROR_DB_ERROR"])) echo CFactory::GetDatabase()->GetError() . "<br>";
?><br>
Can't register: <?php echo $params["login"]; ?><br>
Try again:<br>
<?php CFactory::GetModule("CUser")->ExecTemplate("user-register",null); ?>
</center>