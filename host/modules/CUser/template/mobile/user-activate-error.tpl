<?php
/*
	Name:	CUser register error template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		userdata - user's data from db
		errors - array of errors
	Constants:
		ERROR_ALREADY_ACTIVATE
		ERROR_NOT_FOUND
		ERROR_DB_ERROR
*/
?>

<center>
<?php 
if(isset($params["errors"]["ERROR_ALREADY_ACTIVATE"])) echo "already activated<br>";
if(isset($params["errors"]["ERROR_NOT_FOUND"])) echo "activate key not found<br>";
if(isset($params["errors"]["ERROR_DB_ERROR"])) echo CFactory::GetDatabase()->GetError() . "<br>";
?>
Can't activate: <?php echo $params["userdata"]["login"]; ?>
</center>