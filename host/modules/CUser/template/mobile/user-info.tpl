<?php
/*
	Name:	CUser info template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		userdata - array of user's data from db
*/
?>

<div align=left>
	id: <?php echo $params["userdata"]["id"]; ?><br>
	E-M@il: <?php echo $params["userdata"]["email"]; ?><br>
	Login: <?php echo $params["userdata"]["login"]; ?><br>
	Password: <?php echo $params["userdata"]["password"]; ?><br>
	Registration date: <?php echo $params["userdata"]["reg_date"]; ?><br>
	Last login date: <?php echo $params["userdata"]["last_login"]; ?><br>
	IP: <?php echo $params["userdata"]["ip"]; ?><br>
	Status: <?php echo $params["userdata"]["status"]; ?><br>
	Activate: <?php echo $params["userdata"]["activate"]; ?><br>
	Activation: <?php echo $params["userdata"]["activation"]; ?><br>
	First name: <?php echo $params["userdata"]["first_name"]; ?><br>
	Last name: <?php echo $params["userdata"]["last_name"]; ?><br>
	Birthday: <?php echo $params["userdata"]["birthday"]; ?><br>
	Sex: <?php echo $params["userdata"]["sex"]; ?><br>
	Avatar: <?php echo $params["userdata"]["avatar"]; ?><br>
</div>
