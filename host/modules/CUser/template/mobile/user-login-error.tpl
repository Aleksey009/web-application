<?php
/*
	Name:	CUser login error template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		errors - array of errors
	Errors:
		ERROR_EMPTY_LOGIN
		ERROR_EMPTY_PASSWORD
		ERROR_NOT_FOUND
*/
?>


<center>
<?php 
if(isset($params["errors"]["ERROR_EMPTY_LOGIN"])) echo "write login<br>";
if(isset($params["errors"]["ERROR_EMPTY_PASSWORD"])) echo "write password<br>";
if(isset($params["errors"]["ERROR_NOT_FOUND"])) echo "broken login or password<br>";
?>
Can't login: <?php echo $params["login"]; ?><br>
Try again:<br>
<?php CFactory::GetModule("CUser")->ExecTemplate("user-login",null); ?>
</center>