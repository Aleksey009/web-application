<?php
/*
	Name:	CUser main template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		userdata - array of user's data from db
		islogged - flag of user login
*/
?>

<style type="text/css">
	.user_information
	{
		background: #000000;
		color: #FFFFFF;
	}
</style>

<div title="User information" class="user_information">
<?php 
	if($params["islogged"]) 
	{ 
?>
		id: <?php echo $params["userdata"]["id"]; ?><br>
		Login: <?php echo $params["userdata"]["login"]; ?><br>
		Password: <?php echo $params["userdata"]["password"]; ?><br>
		E-M@il: <?php echo $params["userdata"]["email"]; ?><br>
<?php 
	}
	else echo "not login";
?>
</div>