<?php
/*
	Name: CUser.php
	Author: Alex009 (Михайлов Алексей)
	Description: User module index file.
*/

class CUser extends CModule
{
	// vars
	private $var = 0;				// array of params
	private $users_cache = array();	// array of users data

	// methods
	/*
		name:
			Initialize()
		desc:
			load config
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
	
	}
	/*
		name:
			Install()
		desc:
			install module
		params:
			-
		retn:
			true - complete
			false - fail	
	*/
	public function Install()
	{
		$result = true;
		$db = CFactory::GetDatabase();
		// drop table
		$db->Query("DROP TABLE IF EXISTS `u_user`");
		// create table
		if(!$db->Query("CREATE TABLE `u_user` 
		(
			`id` INT NOT NULL AUTO_INCREMENT ,
			`email` VARCHAR(64) NULL DEFAULT NULL ,
			`login` VARCHAR(32) NULL DEFAULT NULL ,
			`password` VARCHAR(32) NULL DEFAULT NULL ,
			`reg_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
			`last_login` TIMESTAMP NULL DEFAULT NULL ,
			`ip` VARCHAR(16) NULL DEFAULT NULL ,
			`status` TINYINT(1) NULL DEFAULT NULL ,
			`activate` TINYINT(1) NULL DEFAULT NULL ,
			`activation` VARCHAR(32) NULL DEFAULT NULL ,
			`first_name` VARCHAR(64) NULL DEFAULT NULL ,
			`last_name` VARCHAR(64) NULL DEFAULT NULL ,
			`birthday` DATE NULL DEFAULT NULL ,
			`sex` TINYINT(1) NULL DEFAULT NULL ,
			`avatar` VARCHAR(256) NULL DEFAULT NULL ,
			PRIMARY KEY (`id`) 
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `u_user` not created<br>";
			$result = false;
		}
		
		return $result;
	}
	/*
		name:
			Execute($param)
		desc:
			execute module by param
		params:
			$param - param of execution
		retn:
			-
	*/
	public function Execute($param)
	{
		if($param == "main")
		{
			$params = array();
			$params["islogged"] = false;
			if(CFactory::GetSession()->Get("global","userid"))
			{
				$params["userdata"] = $this->GetUserData(CFactory::GetSession()->Get("global","userid"));
				$params["islogged"] = true;
			}
			$this->ExecTemplate("user-main",$params);
		}
		else if($param == "register")
		{
			// check login
			if(CFactory::GetSession()->Get("global","userid"))
			{
				$this->ExecTemplate("user-register-already",null);
			}
			else
			{
				if(!empty($_POST))
				{					
					$db = CFactory::GetDatabase();
					$params = array();
					$params["errors"] = array();
					// check input data
					// email
					if(strlen($_POST["email"]))
					{
						if(!CFactory::GetMailer()->CheckEMail($_POST["email"])) $params["errors"]["ERROR_INVALID_EMAIL"] = true;
					}
					else $params["errors"]["ERROR_EMPTY_EMAIL"] = true;
					// login
					if(strlen($_POST["login"]))
					{
						if(!preg_match("/^[a-zA-Z0-9]/", $_POST["login"])) $params["errors"]["ERROR_INVALID_LOGIN"] = true;
					}
					else $params["errors"]["ERROR_EMPTY_LOGIN"] = true;
					// password
					if(strlen($_POST["password"]))
					{
						if(strlen($_POST["password"]) < 6) $params["errors"]["ERROR_SHORT_PASSWORD"] = true;
					}
					else $params["errors"]["ERROR_EMPTY_PASSWORD"] = true;
					// birthday
					if(!strlen($_POST["birthday"])) $_POST["birthday"] = "0-0-0";
					// check exist registration
					$db->Query("SELECT `email`,`login` 
						FROM `u_user` 
						WHERE `email`=s: OR `login`=s:",$_POST["email"],$_POST["login"]);
					while($userdata = $db->Fetch())
					{
						if($userdata["email"] == $_POST["email"]) $params["errors"]["ERROR_EXIST_EMAIL"] = true;
						if($userdata["login"] == $_POST["login"]) $params["errors"]["ERROR_EXIST_LOGIN"] = true;
					}
					$db->Free();
					// try add
					if(empty($params["errors"]))
					{
						// generate activation key
						$activation = md5(uniqid());
						// generate password hash
						$password_hash = md5($_POST["password"]);
						// insert data into db
						if(!$db->Query("INSERT INTO `u_user` 
							(`email`,`login`,`password`,`reg_date`,`last_login`,`ip`,`activation`,`first_name`,`last_name`,`birthday`,`sex`,`avatar`) 
							VALUES 
							(s:,s:,s:,NOW(),NOW(),s:,s:,s:,s:,s:,i:,s:)",
							$_POST["email"],
							$_POST["login"],
							$password_hash,
							$_SERVER['REMOTE_ADDR'],
							$activation,
							$_POST["first_name"],
							$_POST["last_name"],
							$_POST["birthday"],
							($_POST["sex"]?1:0),
							$_POST["avatar"])) $params["errors"]["ERROR_DB_ERROR"] = true;
					}
					// show needed template
					if(empty($params["errors"]))
					{
						// user added, send mail
						CFactory::GetMailer()->Send(
							$_POST["email"],
							"Activation key",
							"activation: http://cmsdev/user/activate?key=" . $activation
							);
						// template params
						$params["login"] = $_POST["login"];
						// load template
						$this->ExecTemplate("user-register-done",$params);
					}
					else
					{
						// template params
						$params["login"] = $_POST["login"];
						// load template
						$this->ExecTemplate("user-register-error",$params);
					}
				}
				else
				{
					$this->ExecTemplate("user-register",null);
				}
			}
		}
		else if($param == "activate")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// find this activation key
			if(!$db->Query("SELECT * FROM `u_user` WHERE `activation`=s:",$_GET["key"])) $params["errors"]["ERROR_DB_ERROR"] = true;
			if($params["userdata"] = $db->Fetch())
			{
				// check activation
				if($params["userdata"]["activate"]) $params["errors"]["ERROR_ALREADY_ACTIVATE"] = true;
			}
			else $params["errors"]["ERROR_NOT_FOUND"] = true;
			// try activate
			if(empty($params["errors"]))
			{
				// set flag
				if(!$db->Query("UPDATE `u_user` SET `activate`=1 WHERE `id`=i:",$params["userdata"]["id"])) $params["errors"]["ERROR_DB_ERROR"] = true;
			}
			// show template
			if(empty($params["errors"]))
			{
				// load template
				$this->ExecTemplate("user-activate-done",$params);
			}
			else
			{
				// load template
				$this->ExecTemplate("user-activate-error",$params);
			}
		}
		else if($param == "login")
		{
			// check login
			if(CFactory::GetSession()->Get("global","userid"))
			{
				$this->ExecTemplate("user-login-already",null);
			}
			else
			{
				if(!empty($_POST))
				{
					$db = CFactory::GetDatabase();
					$params = array();
					$params["errors"] = array();
					// check input data
					// login
					if(!strlen($_POST["login"])) $params["errors"]["ERROR_EMPTY_LOGIN"] = true;
					// password
					if(!strlen($_POST["password"])) $params["errors"]["ERROR_EMPTY_PASSWORD"] = true;
					// try found data
					if(empty($params["errors"]))
					{
						// generate password hash
						$password_hash = md5($_POST["password"]);
						// find user in base
						$db->Query("SELECT * FROM `u_user` WHERE `login`=s: AND `password`=s:",$_POST["login"],$password_hash);
						if(!($params["userdata"] = $db->Fetch())) $params["errors"]["ERROR_NOT_FOUND"] = true;
					}
					// check errors exists
					if(empty($params["errors"]))
					{
						// set session
						CFactory::GetSession()->Set("global","userid",$params["userdata"]["id"]);
						// update base data
						$db->Query("UPDATE `u_user` SET `last_login`=NOW(), `status`=1, `ip`=s:",$_SERVER['REMOTE_ADDR']);
						// load template
						$this->ExecTemplate("user-login-done",$params);
					}
					else
					{
						$params["login"] = $_POST["login"];
						// load template
						$this->ExecTemplate("user-login-error",$params);
					}
				}
				else
				{
					$this->ExecTemplate("user-login",null);
				}
			}
		}
		else if($param == "logout")
		{
			// check login
			if(!CFactory::GetSession()->Get("global","userid"))
			{
				$this->ExecTemplate("user-logout-error",null);
			}
			else
			{
				CFactory::GetSession()->Reset("global","userid");
				$this->ExecTemplate("user-logout-done",null);
			}
		}
		else if($param == "info")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// find this user
			$db->Query("SELECT * FROM `u_user` WHERE `id`=i:",$_GET["id"]);
			if(!($params["userdata"] = $db->Fetch())) $params["errors"]["ERROR_NOT_FOUND"] = true;
			// show info
			if(empty($params["errors"]))
			{
				// load template
				$this->ExecTemplate("user-info",$params);
			}
			else
			{
				// load template
				$this->ExecTemplate("user-info-error",$params);
			}
		}
		else if($param == "list")
		{
			$db = CFactory::GetDatabase();
			// get users list
			
		}
	}
	/*
		name:
			IsParamAllowed($param)
		desc:
			check param of execution for module
		params:
			$param - param of execution
		retn:
			true or false
	*/
	public function IsParamAllowed($param)
	{
		$allowed = array(
				"register",
				"activate",
				"login",
				"logout",
				"info",
				"list"
			);
		if(in_array($param,$allowed)) return true;
		else return false;
	}
	/*
		name:
			GetUserData($userid)
		desc:
			get user data from array, or if it's not exist - from db
		params:
			$userid - id of user in db
		retn:
			array of userdata or null if user not exists
	*/
	public function GetUserData($userid)
	{
		if(isset($this->users_cache[$userid])) return $this->users_cache[$userid];
		else
		{
			// load data from db
			$result = null;
			$db = CFactory::GetDatabase();	
			$db->Query("SELECT * FROM `u_user` WHERE `id`=i:",$userid);
			if($userdata = $db->Fetch())
			{
				$this->users_cache[$userid] = $userdata;
				$result = $userdata;
			}
			$db->Free();
			return $result;
		}
	}
}
?>