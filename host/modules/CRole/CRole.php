<?php
/*
	Name: CRole.php
	Author: Alex009 (Михайлов Алексей)
	Description: Role module index file.
*/

// constants
define('ROLE_CREATE', "ROLE_CREATE"); 
define('ROLE_EDIT'	, "ROLE_EDIT"); 
define('ROLE_DELETE', "ROLE_DELETE"); 
define('ROLE_LIST'	, "ROLE_LIST"); 
define('ROOT_ROLE'	, "root"); 
define('GHOST_ROLE'	, "ghost"); 


// class
class CRole extends CModule
{
	// vars
	private $var = 0;						// array of params
	private $actions_cache = array();		// actions cache array
	private $role_cache = array();			// role cache array
	private $role_actions_cache = array();	// role actions cache array
	private $user_role_cache = array();		// user roles cache array
	
	// methods
	/*
		name:
			Initialize()
		desc:
			load config
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
		$db = CFactory::GetDatabase();
		// get actions list from db
		if($db->Query("SELECT * FROM `r_action`"))
		{
			while($data = $db->Fetch())
			{
				$this->actions_cache[ $data["name"] ] = $data;
			}
			$db->Free();
		}
		// get roles from db
		if($db->Query("SELECT * FROM `r_role`"))
		{
			while($data = $db->Fetch())
			{
				$this->role_cache[ $data["id"] ] = $data;
			}
			$db->Free();
		}
		// get role actions from db
		if($db->Query("SELECT * FROM `r_role_action_links`"))
		{
			while($data = $db->Fetch())
			{
				$this->role_actions_cache[ $data["roleid"] ][ $data["actionid"] ] = true;
			}
			$db->Free();
		}
	}
	/*
		name:
			Install()
		desc:
			install module
		params:
			-
		retn:
			true - complete
			false - fail
	*/
	public function Install()
	{
		$result = true;
		$db = CFactory::GetDatabase();
		// drop tables
		$db->Query("DROP TABLE IF EXISTS `r_role_user_links`");
		$db->Query("DROP TABLE IF EXISTS `r_role_action_links`");
		$db->Query("DROP TABLE IF EXISTS `r_role`");
		$db->Query("DROP TABLE IF EXISTS `r_action`");
		
		// create tables
		if(!$db->Query("CREATE TABLE `r_role` 
		(
			`id` INT NOT NULL AUTO_INCREMENT ,
			`name` VARCHAR(64) NULL ,
			PRIMARY KEY (`id`) 
		)  DEFAULT CHARSET=utf8")) 
		{
			echo "Table `r_role` not created<br>";
			$result = false;
		}
		// create table
		if(!$db->Query("CREATE TABLE `r_action` 
		(
			`id` INT NOT NULL AUTO_INCREMENT ,
			`name` VARCHAR(64) NOT NULL ,
			`description` VARCHAR(512) NULL ,
			PRIMARY KEY (`id`) 
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `r_action` not created<br>";
			$result = false;
		}
		// create table
		if(!$db->Query("CREATE TABLE `r_role_action_links` 
		(
			`roleid` INT NOT NULL ,
			`actionid` INT NOT NULL ,
			PRIMARY KEY (`roleid`, `actionid`) ,
			CONSTRAINT `r_role_action_links_roleid_fk`
				FOREIGN KEY (`roleid` )
				REFERENCES `r_role` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE,
			CONSTRAINT `r_role_action_links_actionid_fk`
				FOREIGN KEY (`actionid` )
				REFERENCES `r_action` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `r_role_action_links` not created<br>";
			$result = false;
		}
		// create table
		if(!$db->Query("CREATE TABLE `r_role_user_links` 
		(
			`roleid` INT NOT NULL ,
			`userid` INT NOT NULL ,
			PRIMARY KEY (`roleid`, `userid`) ,
			CONSTRAINT `r_role_user_links_roleid_fk`
				FOREIGN KEY (`roleid` )
				REFERENCES `r_role` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE,
			CONSTRAINT `r_role_user_links_userid_fk`
				FOREIGN KEY (`userid` )
				REFERENCES `u_user` (`id` )
				ON DELETE CASCADE
				ON UPDATE CASCADE
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `r_role_user_links` not created<br>";
			$result = false;
		}		
		
		// standart roles
		
		
		// register actions
		$this->RegisterAction(ROLE_CREATE,"Create new role");
		$this->RegisterAction(ROLE_EDIT,"Edit exist role");
		$this->RegisterAction(ROLE_DELETE,"Delete exist role");
		$this->RegisterAction(ROLE_LIST,"See list of exist roles");
		
		return $result;
	}
	/*
		name:
			Execute($param)
		desc:
			execute module by param
		params:
			$param - param of execution
		retn:
			-
	*/
	public function Execute($param)
	{
		if($param == "create")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// get user id
			$userid = CFactory::GetSession()->Get("global","userid");
			// check action
			if(!$this->IsActionAllowedForUser($userid,ROLE_CREATE))
			{
				// set error data
				$params["errors"]["ERROR_NOT_ACCESS"] = true;
				// load template
				$this->ExecTemplate("role-create-error",$params);
				return;
			}
			// check data
			if(!empty($_POST))
			{
				// add new role
				if($db->Query("INSERT INTO `r_role`
						(`name`)
					VALUES
						(s:)",$_POST["name"]))
				{
					// get created role data
					$db->Query("SELECT * FROM `r_role` WHERE `id` = i:",$db->LastInsertId());
					$params["roledata"] = $db->Fetch();
					// load template
					$this->ExecTemplate("role-create-complete",$params);
					return;
				}
				else $params["errors"]["ERROR_DB_ERROR"] = true;
			}
			else
			{
				// load template
				$this->ExecTemplate("role-create",$params);
				return;
			}
			// load template
			$this->ExecTemplate("role-create-error",$params);
			return;
		}
		else if($param == "edit")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// get user id
			$userid = CFactory::GetSession()->Get("global","userid");
			// check action
			if(!$this->IsActionAllowedForUser($userid,ROLE_EDIT))
			{
				// set error data
				$params["errors"]["ERROR_NOT_ACCESS"] = true;
				// load template
				$this->ExecTemplate("role-edit-error",$params);
				return;
			}
			// get editing role
			$roleid = $_GET["id"];
			// check data
			if(!empty($_POST))
			{
				// clear role actions
				$db->Query("DELETE FROM `r_role_action_links` WHERE `roleid`=i:",$roleid);
				// search actions in avaible list
				foreach($this->actions_cache as $name => $data)
				{
					if(!isset($_POST[$name])) continue;
					
					$db->Query("INSERT INTO `r_role_action_links` VALUES (i:,i:)",$roleid,$data["id"]);
				}
				// load template
				$this->ExecTemplate("role-edit-complete",$params);
				return;
			}
			else
			{
				// get role data
				$params["roledata"] = $this->role_cache[ $roleid ];
				// get actions data
				foreach($this->actions_cache as $name => $data)
				{
					// save info
					$newdata["id"] = $data["id"];
					$newdata["name"] = $name;
					$newdata["allowed"] = isset($this->role_actions_cache[ $roleid ][ $data["id"] ]);
					$newdata["description"] = $data["description"];
					// params add
					$params["actiondata"][] = $newdata;
				}
				// load template
				$this->ExecTemplate("role-edit",$params);
				return;
			}
		}
		else if($param == "delete")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// get user id
			$userid = CFactory::GetSession()->Get("global","userid");
			// check action
			if(!$this->IsActionAllowedForUser($userid,ROLE_EDIT))
			{
				// set error data
				$params["errors"]["ERROR_NOT_ACCESS"] = true;
				// load template
				$this->ExecTemplate("role-delete-error",$params);
				return;
			}
			// get deleting role
			$roleid = $_GET["id"];
			// delete links
			$db->Query("DELETE FROM `r_role_action_links` WHERE `roleid`=i:",$roleid);
			// delete role
			$db->Query("DELETE FROM `r_role` WHERE `roleid`=i:",$roleid);
			// load template
			$this->ExecTemplate("role-delete-complete",$params);
			return;
		}
		else if($param == "list")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// get user id
			$userid = CFactory::GetSession()->Get("global","userid");
			// check action
			if(!$this->IsActionAllowedForUser($userid,ROLE_LIST))
			{
				// set error data
				$params["errors"]["ERROR_NOT_ACCESS"] = true;
				// load template
				$this->ExecTemplate("role-list-error",$params);
				return;
			}
			// add roles in params
			$params["roledata"] = $this->role_cache;
			// load template
			$this->ExecTemplate("role-list",$params);
			return;
		}
	}
	/*
		name:
			IsParamAllowed($param)
		desc:
			check param of execution for module
		params:
			$param - param of execution
		retn:
			true or false
	*/
	public function IsParamAllowed($param)
	{
		$allowed = array(
				"create",
				"edit",
				"delete",
				"list"
			);
		if(in_array($param,$allowed)) return true;
		else return false;
	}
	/*
		name:
			RegisterAction($name,$desc)
		desc:
			register new action
		params:
			$name - unique name of action
			$desc - description of action
		retn:
			-
	*/
	public function RegisterAction($name,$desc)
	{
		// not found in cache, add in db
		$db = CFactory::GetDatabase();	
		if($db->Query("INSERT INTO `r_action`
				(`name`,`description`)
			VALUES
				(s:,s:)",$name,$desc));
	}
	/*
		name:
			IsActionAllowedForRole($role,$action)
		desc:
			check allowed action for role or not
		params:
			$role - id of role
			$action - name of action
		retn:
			bool
	*/
	public function IsActionAllowedForRole($role,$action)
	{
		// get action id
		$id = $this->actions_cache[$action]["id"];
		// check in allowed actions
		if(isset($this->role_actions_cache[$role][$id])) return true;
		// not allowed
		return false;
	}
	/*
		name:
			IsActionAllowedForUser($user,$action)
		desc:
			check allowed action for user or not
		params:
			$user - id of user
			$action - name of action
		retn:
			bool
	*/
	public function IsActionAllowedForUser($user,$action)
	{
		// get action id
		if(!isset($this->actions_cache[$action])) return false;
		// check access
		if(!isset($this->user_role_cache[$user]))
		{
			$db = CFactory::GetDatabase();	
			// get user's actions
			if($db->Query("SELECT DISTINCT
				a.name
			FROM
				`r_action` as a,
				`r_role_user_links` as ru,
				`r_role_action_links` as ra
			WHERE
				a.id = ra.actionid
				AND ru.roleid = ra.roleid
				AND ru.userid = i:",$user))
			{
				while($data = $db->Fetch())
				{
					$this->user_role_cache[$user][ $data["name"] ] = true;
				}
				$db->Free();
			}
		}
		if(isset($this->user_role_cache[$user][$action])) return true;
		// not allowed
		return false;
	}
	/*
		name:
			CreateRole($name,$id)
		desc:
			create new role
		params:
			$name - name of role
			$id - id (optional)
		retn:
			-1 - db error
			>= 0 - roleid
	*/
	function CreateRole($name,$id = NULL)
	{
		$db = CFactory::GetDatabase();
		if($id == NULL)
		{
			// just add new role
			if($db->Query("INSERT INTO `r_role` (`name`) VALUES (s:)",$name)) $id = $db->LastInsertId();
			else $id = -1;
		}
		else
		{
			// try add role with custom id
			if(!$db->Query("INSERT INTO `r_role` (`id`,`name`) VALUES (d:,s:)",$id,$name)) $id = -1;
		}
		return $id;
	}
}
?>