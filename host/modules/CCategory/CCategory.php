<?php
/*
	Name: CCategory.php
	Author: Alex009 (Михайлов Алексей)
	Description: Category module index file.
*/

class CCategory extends CModule
{
	// vars
	private $var = 0;				// array of params

	// methods
	/*
		name:
			Initialize()
		desc:
			load config
		params:
			-
		retn:
			-		
	*/
	public function Initialize()
	{
	
	}
	/*
		name:
			Install()
		desc:
			install module
		params:
			-
		retn:
			true - complete
			false - fail
	*/
	public function Install()
	{
		$result = true;
		$db = CFactory::GetDatabase();
		// drop table
		$db->Query("DROP TABLE `c_category`");
		// create table
		if(!$db->Query("CREATE TABLE `c_category` 
		(
			`id` INT NOT NULL ,
			`parentid` INT NULL DEFAULT NULL ,
			`name` VARCHAR(64) NULL DEFAULT NULL ,
			PRIMARY KEY (`id`) ,
			CONSTRAINT `c_category_parentid_fk`
				FOREIGN KEY (`parentid` )
				REFERENCES `c_category` (`id` )
				ON DELETE NO ACTION
				ON UPDATE NO ACTION
		) DEFAULT CHARSET=utf8")) 
		{
			echo "Table `c_category` not created<br>";
			$result = false;
		}
		
		return $result;
	}
	/*
		name:
			Execute($param)
		desc:
			execute module by param
		params:
			$param - param of execution
		retn:
			-
	*/
	public function Execute($param)
	{
		if($param == "main")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// get category list
			if($db->Query("SELECT *
				FROM `c_category`
				ORDER BY `parentid`"))
			{
				while($data = $db->Fetch())
				{
					$params["categories"][] = $data;
				}
			}
			else $params["errors"]["ERROR_DB_ERROR"];
			
			if(empty($params["errors"]))
			{
				$this->ExecTemplate("category-main",$params);
			}
			else
			{
				$this->ExecTemplate("category-error",$params);
			}			
		}
		else if($param == "info")
		{
			$db = CFactory::GetDatabase();
			$params = array();
			$params["errors"] = array();
			// category id
			$params["categoryid"] = 0;
			if(isset($_GET["id"])) $params["categoryid"] = $_GET["id"];
			
		}
	}
	/*
		name:
			IsParamAllowed($param)
		desc:
			check param of execution for module
		params:
			$param - param of execution
		retn:
			true or false
	*/
	public function IsParamAllowed($param)
	{
		$allowed = array(
				"info"
			);
		if(in_array($param,$allowed)) return true;
		else return false;
	}
}
?>