<?php
/*
	Name:	Content page template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		content_module - current selected module
		content_param - current param of selected module
*/

CFactory::GetDocument()->AddCss({tfolder="css/main.css"});
?>

<div class="content">
	<div class="header">
		<h1>
			Alex009's blog
		</h1>
	</div>
	<div class="tabs">
		<div class="tabs_inner">
			<ul>
				<li><a href="/user/register/">Register</a>
				<li><a href="/user/login/">Login</a>
				<li><a href="/user/info?id=0">Info</a>
				<li><a href="/user/logout/">Logout</a>
				<li><a href="/news/add/">Add</a>
				<li><a href="/news/main/">List</a>
			</ul>
		</div>
	</div>
	<div class="main">
		<div class="main_left">
			<?php 
				CFactory::GetModule($params["content_module"])->Execute($params["content_param"]); 
			?>
		</div>
		<div class="main_right">
Alex009 / 009<br>
Михайлов Алексей<br>
<br>
ICQ: 5306763<br>
Skype: Aleksey.Mikhailov.ru<br>
		</div>
	</div>
	<div class="bottom">
		Design by Alex009
	</div>
</div>