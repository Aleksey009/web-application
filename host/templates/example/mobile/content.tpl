<?php
/*
	Name:	Content page template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		content_module - current selected module
		content_param - current param of selected module
*/
?>

<table width="100%" border=1>
	<tr>
		<td width=100% valign=top align=center colspan=3>
			<img alt="Smiley face" src={tfolder="img/test.jpg"} height="100" width="100">
			<?php 
				CFactory::GetModule("CUser")->Execute("main"); 
			?>
		</td>
	</tr>
	<tr>
		<td width=20% valign=top>
			<ol type="I">
				<li><b>User:</b>
				<ol type="I">
					<li><a href="/user/register/">Register</a>
					<li><a href="/user/login/">Login</a>
					<li><a href="/user/info?id=0">Info</a>
					<li><a href="/user/logout/">Logout</a>
				</ol>
				<li><b>News:</b>
				<ol type="I">
					<li><a href="/news/add/">Add</a>
					<li><a href="/news/main/">List</a>
				</ol>
			</ol>
		</td>
		<td width=60% valign=top>
			<div id="content">
				<?php 
					CFactory::GetModule($params["content_module"])->Execute($params["content_param"]); 
				?>
			</div>
		</td>
		<td width=20% valign=top>
			right column
		</td>
	</tr>
</table>