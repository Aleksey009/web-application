<?php
/*
	Name:	Closed archive control log read template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		logsize - size of log
		logs - array data from db:
			`fileid`
			`filename`
			`filehash`
			`filepath`
			`username`
			`usercode`
			`ip`
			`moment`
*/
?>
<div class="data_block">
<?php if($params["logsize"] == 0): ?>
	<center>
		Лог пуст.
	</center>
<?php else: ?>

<?php foreach($params["logs"] as $i => $data): ?>
	<table class="table_dotted">
		<tr>
			<td>Время:</td>
			<td><?php echo $data["moment"]; ?></td>
		</tr>
		<tr>
			<td>Скачал:</td>
			<td><?php echo $data["username"]; ?> - <?php echo $data["ip"]; ?></td>
		</tr>
		<tr>
			<td>Файл:</td>
			<td><?php echo $data["filename"]; ?> - <?php echo $data["filepath"]; ?></td>
		</tr>
		<tr>
			<td><?php echo $data["filehash"]; ?></td>
			<td><?php echo $data["usercode"]; ?></td>
		</tr>
	</table>
	<hr>
<?php endforeach; ?>

<?php endif; ?>	
</div>