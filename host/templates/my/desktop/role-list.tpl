<?php
/*
	Name:	CRole list template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		-
*/
?>

<div class="data_block">
	List of roles:<br>
	<?php foreach($params["roledata"] as $i => $data): ?>
		<a href="/role/edit?id=<?php echo $data["id"]; ?>"><?php echo $data["name"]; ?></a><br>
	<?php endforeach; ?>
</div>
