<?php
/*
	Name:	CRole edit error template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		userdata - array of user's data from db
		errors - array of errors
	Errors:
		ERROR_NOT_LOGIN
		ERROR_NOT_ACCESS
*/
?>
<div class="data_block">
	Errors:
	<?php 
		if(isset($params["errors"]["ERROR_NOT_LOGIN"])) echo "You not login<br>";	
		if(isset($params["errors"]["ERROR_NOT_ACCESS"])) echo "You not have access<br>";
	?>
</div>
