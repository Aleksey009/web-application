<?php
/*
	Name:	Closed archive control user edit template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		userdata - array of user data:
			`id`
			`name`
			`code`
*/
?>
<div class="data_block">
	<form method="post">
		<table class="table_hidden" width="100%">
			<tr>
				<td width="30%">Имя пользователя:</td>
				<td><input name="name" type="text" size="33" maxlength="64" value="<?php echo $params["userdata"]["name"]; ?>"></td>
			</tr>
			<tr>
				<td>Код доступа:</td>
				<td><input name="code" type="text" size="33" maxlength="32" value="<?php echo $params["userdata"]["code"]; ?>"></td>
			</tr>
		</table>
		<center><input type="submit" name="submit" value="Сохранить"></center>
	</form>
</div>