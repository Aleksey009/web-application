<?php
/*
	Name:	News main template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		newsdata - array data from db:
			`id`
			`date`
			`header`
			`short_text`
			`author_id`
			`author_login`	
		start - start news
		per_page - number per page
		news_count - number of news
		showed_news - news on this page
*/
?>

<?php foreach($params["newsdata"] as $i => $data): ?>
	<div class="data_block">
		<div class="news_header">
			<a href="/news/read?id=<?php echo $data["id"];?>"><?php echo $data["header"];?></a><span><?php echo $data["date"];?></span>
		</div>
		<div class="news_text">
			<?php echo $data["short_text"];?>
		</div>
		<div class="news_bottom">
			Author: <?php echo $data["author_login"];?>
			<span>Category: <?php echo $data["cat_name"];?></span>
		</div>
	</div>
<?php endforeach; ?>

<div class="data_block">
	<div class="news_page">
		<?php 
			// paginator... 
			$start = intval($params["start"] / $params["per_page"]);
			$backcount = ceil($params["start"] / $params["per_page"]);
			$upcount = ceil(($params["news_count"] - $params["start"]) / $params["per_page"]) - 1;
			if($backcount < 5)
			{
				for($i = 0;$i < $backcount;$i++) echo "<a href=\"?start=" . ($i * $params["per_page"]) . "\">" . ($i + 1) . "</a>\n";
			}
			else
			{
				for($i = 0;$i < 2;$i++) echo "<a href=\"?start=" . ($i * $params["per_page"]) . "\">" . ($i + 1) . "</a>\n";
				echo " ... ";
				for($i = ($backcount - 2);$i < $backcount;$i++) echo "<a href=\"?start=" . ($i * $params["per_page"]) . "\">" . ($i + 1) . "</a>\n";
			}
			echo "<span>" . ($start + 1) . "</span>\n";
			if($upcount < 5)
			{
				$upcount += $start;
				for($i = $start + 1;$i <= $upcount;$i++) echo "<a href=\"?start=" . ($i * $params["per_page"]) . "\">" . ($i + 1) . "</a>\n";
			}
			else
			{
				$upcount += $start;
				for($i = $start + 1;$i < ($start + 3);$i++) echo "<a href=\"?start=" . ($i * $params["per_page"]) . "\">" . ($i + 1) . "</a>\n";
				echo " ... ";
				for($i = ($upcount - 1);$i <= $upcount;$i++) echo "<a href=\"?start=" . ($i * $params["per_page"]) . "\">" . ($i + 1) . "</a>\n";
			}
		?>
	</div>
</div>