<?php
/*
	Name:	Closed archive control file edit template
	Author:	Alex009 (Михайлов Алексей)
	Input:
		filedata - array data from db:
			`id`
			`name`
			`version`
			`date`
			`author`
			`description`
			`hash`
			`filename`
*/
?>
<div class="data_block">
	<form method="post">
		<table class="table_hidden" width="100%">
			<tr>
				<td width="30%">Название файла:</td>
				<td><input name="name" type="text" size="33" maxlength="64" value="<?php echo $params["filedata"]["name"]; ?>"></td>
			</tr>
			<tr>
				<td>Версия:</td>
				<td><input name="version" type="text" size="17" maxlength="16" value="<?php echo $params["filedata"]["version"]; ?>"></td>
			</tr>
			<tr>
				<td>Автор:</td>
				<td><input name="author" type="text" size="33" maxlength="64" value="<?php echo $params["filedata"]["author"]; ?>"></td>
			</tr>
			<tr>
				<td>Описание:</td>
				<td><textarea name="description"><?php echo $params["filedata"]["description"]; ?></textarea></td>
			</tr>
		</table>
		<center><input type="submit" name="submit" value="Сохранить"></center>
	</form>
</div>