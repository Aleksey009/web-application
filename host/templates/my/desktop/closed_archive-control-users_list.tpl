<?php
/*
	Name:	Closed archive users list template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		userscount - count of users
		usersdata - array data from db:
			`id`
			`name`
			`code`
*/
?>
<?php if($params["userscount"] == 0): ?>
<div class="data_block">
	<center>
		Нет пользователей закрытого архива.
	</center>
</div>
<?php else: ?>

<div class="data_block">
	<table class="table_dotted" width="100%">
<?php foreach($params["usersdata"] as $i => $data): ?>
		<tr>
			<td>
				<table class="table_hidden" width="100%">
					<tr>
						<td width="30%">Имя пользователя:</td>
						<td><?php echo $data["name"]; ?></td>
					</tr>
					<tr>
						<td>Код доступа:</td>
						<td><tt><?php echo $data["code"]; ?></tt></td>
					</tr>
					<tr>
						<td colspan=2>
							<a href="/ca/control?action=user_edit&id=<?php echo $data["id"];?>">Изменить</a>
							<a href="/ca/control?action=user_delete&id=<?php echo $data["id"];?>">Удалить</a>
							<a href="/ca/control?action=user_files&id=<?php echo $data["id"];?>">Доступные файлы</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
<?php endforeach; ?>
	</table>
</div>

<?php endif; ?>