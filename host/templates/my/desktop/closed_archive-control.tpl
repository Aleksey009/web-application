<?php
/*
	Name:	Closed archive control template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		actions - array of allowed actions:
			CA_USER_CREATE
			CA_USER_EDIT
			CA_USER_DELETE
			CA_FILE_CREATE
			CA_FILE_EDIT
			CA_FILE_DELETE
			CA_LOG_READ
			CA_USERS_LIST
			CA_FILES_LIST
			CA_USER_FILES
*/
?>
<div class="data_block">
	<?php if(isset($params["actions"]["CA_USER_CREATE"])): ?>
	<a href="/ca/control?action=user_create">Создать пользователя</a><br>
	<?php endif; ?>
	<?php if(isset($params["actions"]["CA_USERS_LIST"])): ?>
	<a href="/ca/control?action=users_list">Список пользователей</a><br>
	<?php endif; ?>
	<?php if(isset($params["actions"]["CA_FILE_CREATE"])): ?>
	<a href="/ca/control?action=file_create">Создать файл</a><br>
	<?php endif; ?>
	<?php if(isset($params["actions"]["CA_FILES_LIST"])): ?>
	<a href="/ca/control?action=files_list">Список файлов</a><br>
	<?php endif; ?>
	<?php if(isset($params["actions"]["CA_LOG_READ"])): ?>
	<a href="/ca/control?action=log_read">Просмотреть лог</a><br>
	<?php endif; ?>
</div>