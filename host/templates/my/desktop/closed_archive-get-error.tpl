<?php
/*
	Name:	Closed archive get error template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		errors - array of errors
	Errors:
		ERROR_ROW_NOT_FOUND
		ERROR_DB_ERROR
*/
?>
<div class="data_block">
	<?php if(isset($params["errors"]["ERROR_DB_ERROR"])): ?>
	Ошибка базы данных, сообщите администратору.
	<?php endif; ?>
	<?php if(isset($params["errors"]["ERROR_ROW_NOT_FOUND"])): ?>
	Запись не найдена.
	<?php endif; ?>
</div>