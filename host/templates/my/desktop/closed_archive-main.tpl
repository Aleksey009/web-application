<?php
/*
	Name:	Closed archive main template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		-
*/
?>

<div class="data_block">
	<p>
		Данный раздел - закрытый архив. Используя специальный код можно получить доступ к архиву, в нем содержатся последние версии проектов, доступных по этому коду.
		<center><a href="ca/list">Перейти в архив</a></center>
	<p>
</div>