<?php
/*
	Name:	Closed archive control error template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		errors - array of errors
	Errors:
		ERROR_NOT_LOGIN
		ERROR_NOT_ACCESS
		ERROR_DB_ERROR
*/
?>
<div class="data_block">
	<?php if(isset($params["errors"]["ERROR_DB_ERROR"])): ?>
	Ошибка базы данных, сообщите администратору. <?php echo CFactory::GetDatabase()->GetError(); ?>
	<?php endif; ?>
	<?php if(isset($params["errors"]["ERROR_NOT_LOGIN"])): ?>
	Раздел доступен только зарегистрированным пользователям, выполните вход.
	<?php endif; ?>
	<?php if(isset($params["errors"]["ERROR_NOT_ACCESS"])): ?>
	У вас не достаточно привелегий для использования раздела.
	<?php endif; ?>
</div>