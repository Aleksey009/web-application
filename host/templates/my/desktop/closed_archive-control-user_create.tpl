<?php
/*
	Name:	Closed archive control user create template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		
*/

CFactory::GetDocument()->AddJavaScript({tfolder="js/closed_archive-control.js"});
?>
<div class="data_block">
	<form method="post" id="user_create">
		<table class="table_hidden" width="100%">
			<tr>
				<td width="30%">Имя пользователя:</td>
				<td><input name="name" type="text" size="33" maxlength="64"></td>
			</tr>
			<tr>
				<td>Код доступа:</td>
				<td><input name="code" type="text" size="33" maxlength="32"><input type="button" value="Сгенерировать" onclick="generate_code('user_create')"/></td>
			</tr>
		</table>
		<center><input type="submit" name="submit" value="Создать"></center>
	</form>
</div>