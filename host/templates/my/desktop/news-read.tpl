<?php
/*
	Name:	News read template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		newsdata - array data from db:
			`id`
			`date`
			`header`
			`cat_id`
			`cat_name`
			`short_text`
			`text`
			`author_id`
			`author_login`	
*/
?>

<div class="data_block">
	<div class="news_header">
		<a href="/news/read?id=<?php echo $params["newsdata"]["id"];?>"><?php echo $params["newsdata"]["header"];?></a><span><?php echo $params["newsdata"]["date"];?></span>
	</div>
	<div class="news_text">
		<?php echo $params["newsdata"]["text"];?>
	</div>
	<div class="news_bottom">
		Author: <?php echo $params["newsdata"]["author_login"];?>
		<span>Category: <?php echo $params["newsdata"]["cat_name"];?></span>
	</div>
</div>