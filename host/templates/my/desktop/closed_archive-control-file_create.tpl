<?php
/*
	Name:	Closed archive control user create template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		
*/
?>
<div class="data_block">
	<form enctype="multipart/form-data" method="post">
		<table class="table_hidden" width="100%">
			<tr>
				<td width="30%">Название файла:</td>
				<td><input name="name" type="text" size="33" maxlength="64"></td>
			</tr>
			<tr>
				<td>Версия:</td>
				<td><input name="version" type="text" size="17" maxlength="16"></td>
			</tr>
			<tr>
				<td>Автор:</td>
				<td><input name="author" type="text" size="33" maxlength="64"></td>
			</tr>
			<tr>
				<td>Описание:</td>
				<td><textarea name="description" width="100%"></textarea></td>
			</tr>
			<tr>
				<td>Файл для загрузки:</td>
				<td>
					<input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
					<input name="userfile" type="file" />
				</td>
			</tr>
		</table>
		<center><input type="submit" name="submit" value="Создать"></center>
	</form>
</div>