<?php
/*
	Name:	CRole edit template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		-
*/
?>

<div class="data_block">
	Edit role <?php echo $params["roledata"]["name"]; ?><br>
	<form method="post">
		<?php foreach($params["actiondata"] as $i => $data): ?>
			<label><?php echo $data["description"]; ?>:</label> <input name="<?php echo $data["name"]; ?>" type="checkbox" <?php if($data["allowed"]) echo "checked"; ?>><br>
		<?php endforeach; ?>
		<input type="submit" name="submit" value="Accept">
	</form>
</div>
