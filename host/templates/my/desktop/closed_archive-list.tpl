<?php
/*
	Name:	Closed archive list input template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		count - count of files
		data - array data from db:
			`id`
			`name`
			`version`
			`date`
			`author`
			`description`
			`hash`
			`filename`
*/
?>
<?php if($params["count"] == 0): ?>
<div class="data_block">
	<center>
		По данному коду нет доступных файлов.
	</center>
</div>
<?php else: ?>

<?php foreach($params["data"] as $i => $data): ?>
	<div class="data_block">
		<table class="table_dotted" width="100%">
			<tr>
				<td width="25%">Название:</td>
				<td width="75%"><?php echo $data["name"];?></td>
			</tr>
			<tr>
				<td>Версия:</td>
				<td><?php echo $data["version"];?></td>
			</tr>
			<tr>
				<td>Дата добавления:</td>
				<td><?php echo $data["date"];?></td>
			</tr>
			<tr>
				<td>Автор:</td>
				<td><?php echo $data["author"];?></td>
			</tr>
			<tr>
				<td>MD5:</td>
				<td><?php echo $data["hash"];?></td>
			</tr>
			<tr>
				<td>Описание:</td>
				<td><?php echo $data["description"];?></td>
			</tr>
			<tr>
				<td colspan=2>
					<center><a href="/ca/get?id=<?php echo $data["id"]; ?>">Скачать</a></center>
				</td>
			</tr>
		</table>		
	</div>
<?php endforeach; ?>

<?php endif; ?>