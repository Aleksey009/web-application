<?php
/*
	Name:	Closed archive files list template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		filescount - count of files
		filesdata - array data from db:
			`id`
			`name`
			`version`
			`date`
			`author`
			`description`
			`hash`
			`filename`
*/
?>
<?php if($params["filescount"] == 0): ?>
<div class="data_block">
	<center>
		Нет файлов в закрытом архиве.
	</center>
</div>
<?php else: ?>

<div class="data_block">
	<table class="table_dotted" width="100%">
<?php foreach($params["filesdata"] as $i => $data): ?>
		<tr>
			<td>
				<table class="table_hidden" width="100%">
					<tr>
						<td width="30%">Название:</td>
						<td><?php echo $data["name"]; ?></td>
					</tr>
					<tr>
						<td>Версия:</td>
						<td><?php echo $data["version"]; ?></td>
					</tr>
					<tr>
						<td>Дата добавления:</td>
						<td><?php echo $data["date"]; ?></td>
					</tr>
					<tr>
						<td>Автор:</td>
						<td><?php echo $data["author"]; ?></td>
					</tr>
					<tr>
						<td>Описание:</td>
						<td><?php echo $data["description"]; ?></td>
					</tr>
					<tr>
						<td>Hash:</td>
						<td><?php echo $data["hash"]; ?></td>
					</tr>
					<tr>
						<td>Файл:</td>
						<td><?php echo $data["filename"]; ?></td>
					</tr>
					<tr>
						<td align="center"><a href="/ca/control?action=file_edit&id=<?php echo $data["id"];?>">Изменить</a></td>
						<td align="center"><a href="/ca/control?action=file_delete&id=<?php echo $data["id"];?>">Удалить</a></td>
					</tr>
				</table>
			</td>
		</tr>
<?php endforeach; ?>
	</table>
</div>

<?php endif; ?>