<?php
/*
	Name:	Closed archive control user files template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		filescount - count of files
		filesdata - array data from db:
			`id`
			`name`
			`version`
			`date`
			`author`
			`description`
			`hash`
			`filename`
		start - start id of file
		per_page - files per page
		userfiles - array of id files allowed for user
*/
?>
<?php if($params["filescount"] == 0): ?>
<div class="data_block">
	<center>
		Нет файлов в закрытом архиве.
	</center>
</div>
<?php else: ?>

<div class="data_block">
	<form method="post">
		<table class="table_dotted" width="100%">
	<?php foreach($params["filesdata"] as $i => $data): ?>
			<tr>
				<td>
					<table class="table_hidden" width="100%">
						<tr>
							<td width="30%">Название:</td>
							<td><?php echo $data["name"]; ?></td>
						</tr>
						<tr>
							<td>Дата добавления:</td>
							<td><?php echo $data["date"]; ?></td>
						</tr>
						<tr>
							<td>Файл:</td>
							<td><?php echo $data["filename"]; ?></td>
						</tr>
					</table>
				</td>
				<td align="center">
					<input name="all_files[<?php echo $data["id"]; ?>]" type="hidden" value="<?php if(isset($params["userfiles"][ $data["id"] ])) echo "1"; else echo "0"; ?>"/>
					<input name="files[<?php echo $data["id"]; ?>]" type="checkbox" <?php if(isset($params["userfiles"][ $data["id"] ])) echo "checked"; ?>>
				</td>
			</tr>
	<?php endforeach; ?>
		</table>
		<center><input type="submit" name="submit" value="Сохранить"></center>
	</form>
</div>

<?php endif; ?>