<?php
/*
	Name:	Content page template
	Author:	Alex009 (Михайлов Алексей)
	Input:	
		content_module - current selected module
		content_param - current param of selected module
*/

CFactory::GetDocument()->AddCss({tfolder="css/main.css"});
?>
<div class="content">
	<div class="header">
		<h1>
			Alex009's blog
		</h1>
	</div>
	<div class="tabs">
		<div class="tabs_inner">
			<ul>
				<li><a href="/news/main/">Новости</a>
				<li><a href="/ca/main/">Архив</a>
			</ul>
		</div>
	</div>
	<div class="main">
		<div class="main_left">
			<?php CFactory::GetModule($params["content_module"])->Execute($params["content_param"]); ?>
		</div>
		<div class="main_right">
			<div class="data_block">
				<h1>Обо мне</h1>
				Alex009 / 009<br>
				Михайлов Алексей<br>
				<br>
				e-mail: Aleksey.Mikhailov.ru@gmail.com<br>
				Skype: Aleksey.Mikhailov.ru
			</div>
		</div>
	</div>
	<div class="bottom">
		Maded by Alex009
	</div>
</div>